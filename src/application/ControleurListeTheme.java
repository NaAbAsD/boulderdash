package application;

import java.io.File;
import java.io.FileFilter;

import javafx.fxml.FXML;
import javafx.scene.control.ListView;

/**
 * <b>ControleurListeTheme est la classe repr�sentant le controleur de la fenetre de la liste des th�mes du Boulderdash.</b>
 * <p>
 * Ce controleur est caract�ris� par l'information suivante :
 * <ul>
 * <li>Une ListView d�fini dans un fichier FXML, cette liste affiche les diff�rents th�mes du jeu.</li>
 * </ul>
 * </p>
 */
public class ControleurListeTheme {
	
	/**
	 * ListView affichant les diff�rents th�mes de jeu disponible.
	 */
	@FXML ListView<String> _listView;

	/**
	 * Constructeur de ControleurListeTheme
	 */
	public ControleurListeTheme() {}
	
	/**
     * Initialize ControleurListeTheme.
     * <p>
     * A la construction d'un objet ControleurListeTheme,
     * On met � jour la liste de th�me.
     * </p>
     * 
     * @see ControleurListeTheme#ajouterListe()
     */
	@FXML
	public void initialize() {
		ajouterListe();
	}
	
	/**
	 * La Fonction ajouterListe ajoute � la ListView les fichiers dont le nom commence par "Theme" et fini par un nombre.
	 * 
	 * @see ControleurListeTheme#_listView
	 * @see ControleurListeTheme#isNumber(String)
	 */
	public void ajouterListe() {
		File[] directories = new File("ressources/images").listFiles(new FileFilter() {
		    @Override
		    public boolean accept(File file) {
		    	String name = file.getName();
		    	String number = file.getName().substring(5, name.length());
		    	return file.isDirectory() && name.startsWith("Theme") && isNumber(number);
		    }
		});
		for (File f: directories) {
			_listView.getItems().add(f.getName());
		}
	}
	
	/**
	 * Retourne un bool�en si le String entr� en param�tre est un nombre
	 * 
	 * @param number
	 * 			Le String de la fin du fichier
	 * 
	 * @return un bool�en qui est vrai si le String (qui est la deuxieme partie du fichier) est un nombre.
	 */
	public boolean isNumber(String number) {
		try {
  		  Integer.parseInt(number);
  		  return true;
  		} catch (NumberFormatException e) {
  			return false;
  		}
	}
	
	/**
	 * Retourne la partie enti�re du String s�l�ctionn� dans la listView
	 * 
	 * @return un entier qui est le nombre du th�me s�lectionn�
	 * 
	 * @see ControleurListeTheme#_listView
	 */
	public int valider() {
		String name = _listView.getSelectionModel().getSelectedItem();
		int num = Integer.parseInt(name.substring(5, name.length()));
		return num;
	}
	
	/**
	 * Retourne -1 pour montrer l'annulation du th�me
	 * 
	 * @return -1
	 */
	public int quitter() {
		return -1;
	}
	
}
