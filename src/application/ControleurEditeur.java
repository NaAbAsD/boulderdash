package application;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Optional;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.HPos;
import javafx.geometry.VPos;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import modele.plateau.Grille;
import modele.plateau.objets.Acier;
import modele.plateau.objets.Diamant;
import modele.plateau.objets.Monstre;
import modele.plateau.objets.Objet;
import modele.plateau.objets.Rocher;
import modele.plateau.objets.Rockford;
import modele.plateau.objets.Sortie;
import modele.plateau.objets.Terre;
import modele.plateau.objets.Vide;

/**
 * <b>ControleurEditeur est la classe repr�sentant le controleur de la fenetre d'�diteur de niveaux du Boulderdash.</b>
 * <p>
 * Ce controleur est caract�ris� par les informations suivantes :
 * <ul>
 * <li>Une GridPane d�fini dans un fichier FXML, cette GridPane repr�sente la map � modifier.</li>
 * <li>Huit boutons d�finis dans un fichier FXML, ces boutons servent � choisir le type d'objet � ajouter dans la GridPane.</li>
 * <li>Une Grille permettant de sauvegarder les choix fait dans la GridPane afin d'ecrire la Grille sur un fichier texte.</li>
 * <li>Une instance de Rockford permettant de modifier la position de Rockford.</li>
 * <li>Une ImageView repr�sentant l'imageView de la position de Rockford.</li>
 * <li>Un bool�en permettant de savoir si l'utilisateur souhaite positionn� un Rockford sur la Grille.</li>
 * <li>Un tableau de deux entiers, permettant de savoir la position de l'Objet Sortie sur la Grille.</li>
 * <li>Une ImageView repr�sentant l'imageView de la position de l'Objet Sortie.</li>
 * <li>Un bool�en permettant de savoir si la Sortie est actuellement initialis� sur la Grille.</li>
 * <li>Un Objet repr�sentant l'Objet � initialiser sur la map lorsque l'on modifie la Grid.</li>
 * <li>Une image repr�sentant l'image de l'Objet � afficher dans la GridPane � la position souhait�e.</li>
 * </ul>
 * </p>
 * 
 * @see Grille
 * @see Rockford
 * @see Objet
 */
public class ControleurEditeur {

	/**
	 * Une GridPane d�fini dans un fichier FXML, cette GridPane repr�sente la map � modifier.
	 */
	@FXML private GridPane _grid;
	
	/**
	 * Les boutons d�finis dans un fichier FXML, permettant de choisir le type d'objet � ajouter dans la GridPane.
	 */
	@FXML private Button _boutonRockFord;
	@FXML private Button _boutonVide;
	@FXML private Button _boutonTerre;
	@FXML private Button _boutonRocher;
	@FXML private Button _boutonMonstre;
	@FXML private Button _boutonDiamant;
	@FXML private Button _boutonAcier;
	@FXML private Button _boutonSortie;

	/**
	 * Une Grille permettant de sauvegarder les choix fait dans la GridPane afin d'ecrire la Grille sur un fichier texte.
	 * 
	 * @see Grille
	 * @see Grille#HAUTEUR
	 * @see Grille#LARGEUR
	 * 
	 * @see ControleurEditeur#getGrille()
	 * @see ControleurEditeur#setGrille()
	 */
	private Grille grille = new Grille(Grille.HAUTEUR, Grille.LARGEUR);

	/**
	 * Une instance de Rockford permettant de modifier la position de Rockford.
	 * 
	 * @see Rockford
	 * 
	 * @see ControleurEditeur#getPlayer()
	 * @see ControleurEditeur#setPlayer(Rockford)
	 */
	private Rockford player = new Rockford();
	
	/**
	 * Une ImageView repr�sentant l'imageView de la position de Rockford.
	 * 
	 * @see ControleurEditeur#getIwRockford()
	 * @see ControleurEditeur#setIwRockford(ImageView)
	 */
	private ImageView iwRockford;
	
	/**
	 * Le bool�en permettant de savoir si l'utilisateur a s�l�ctionn� le bouton de Rockford.
	 * 
	 * @see ControleurEditeur#isRockfordSelected()
	 * @see ControleurEditeur#setRockfordSelected(boolean)
	 */
	private boolean rockfordSelected = false;
	
	/**
	 * Un tableau de deux entiers, permettant de savoir la position de l'Objet Sortie sur la Grille.
	 * 
	 * @see ControleurEditeur#getPosSortie()
	 * @see ControleurEditeur#setPosSortie(int[])
	 */
	private int[] posSortie = new int[2];
	
	/**
	 * Une ImageView repr�sentant l'imageView de la position de l'Objet Sortie.
	 * 
	 * @see ControleurEditeur#getIwSortie()
	 * @see ControleurEditeur#setIwSortie(ImageView)
	 */
	private ImageView iwSortie;
	
	/**
	 * Le bool�en permettant de savoir si il y a une Sortie d'initialis�e.
	 * 
	 * @see ControleurEditeur#isSortieSelected()
	 * @see ControleurEditeur#setSortieSelected(boolean)
	 */
	private boolean sortieSelected = false;
	
	/**
	 * Un Objet repr�sentant l'Objet � initialiser sur la map lorsque l'on modifie la Grid.
	 * 
	 * @see ControleurEditeur#getObj()
	 * @see ControleurEditeur#setObj(Objet)
	 */
	private Objet obj;
	
	/**
	 * Une image repr�sentant l'image de l'Objet � afficher dans la GridPane � la position souhait�e.
	 * 
	 * @see ControleurEditeur#getImageChoisie()
	 * @see ControleurEditeur#setImageChoisie(Image)
	 */
	private Image imageChoisie;
	
	/**
	 * Constructeur de ControleurEditeur
	 */
	public ControleurEditeur() {}
	
	/**
	 * Retourne la Grille de ControleurEditeur
	 * 
	 * @return une instance de Grille
	 * 
	 * @see Grille
	 * 
	 * @see ControleurEditeur#grille
	 */
	public Grille getGrille() {
		return grille;
	}

	/**
	 * Met � jour la grille de ControleurEditeur
	 * 
	 * @param grille
	 * 
	 * @see ControleurEditeur#grille
	 */
	public void setGrille(Grille grille) {
		this.grille = grille;
	}
	
	/**
	 * Retourne le Rockford de ControleurEditeur
	 * 
	 * @return une instance de Rockford
	 * 
	 * @see Rockford
	 * 
	 * @see ControleurEditeur#player
	 */
	public Rockford getPlayer() {
		return player;
	}

	/**
	 * Met � jour le Rockford de ControleurEditeur
	 * 
	 * @param player
	 * 
	 * @see ControleurEditeur#player
	 */
	public void setPlayer(Rockford player) {
		this.player = player;
	}
	
	/**
	 * Retourne l'ImageView du Rockford de ControleurEditeur
	 * 
	 * @return une instance de l'imageView de Rockford
	 * 
	 * @see ControleurEditeur#iwRockford
	 */
	public ImageView getIwRockford() {
		return iwRockford;
	}

	/**
	 * Met � jour l'ImageView du Rockford de ControleurEditeur
	 * 
	 * @param iwRockford
	 * 
	 * @see ControleurEditeur#iwRockford
	 */
	public void setIwRockford(ImageView iwRockford) {
		this.iwRockford = iwRockford;
	}
	
	/**
	 * Retourne le bool�en qui permet de savoir si Rockford est s�l�ctionn� par l'utilisateur
	 * 
	 * @return un bool�en 
	 *
	 * @see ControleurEditeur#rockfordSelected
	 */
	public boolean isRockfordSelected() {
		return rockfordSelected;
	}

	/**
	 * Met � jour l'�tat du bool�en qui permet de savoir si l'utilisateur a s�l�ctionn� le bouton de Rockford
	 * 
	 * @param rockfordSelected
	 * 
	 * @see ControleurEditeur#rockfordSelected
	 */
	public void setRockfordSelected(boolean rockfordSelected) {
		this.rockfordSelected = rockfordSelected;
	}
	
	/**
	 * Retourne de tableau d'entier permettant de connaitre la position de la Sortie
	 * 
	 * @return un tableau d'entier repr�sentant une position (x,y)
	 * 
	 * @see ControleurEditeur#posSortie
	 */
	public int[] getPosSortie() {
		return posSortie;
	}

	/**
	 * Met � jour la position de la Sortie sur la Grille/GridPane
	 * 
	 * @param posSortie
	 * 
	 * @see ControleurEditeur#posSortie
	 */
	public void setPosSortie(int[] posSortie) {
		this.posSortie = posSortie;
	}

	/**
	 * Retourne l'ImageView de la Sortie de ControleurEditeur
	 * 
	 * @return une instance de l'imageView de Sortie
	 * 
	 * @see ControleurEditeur#iwSortie
	 */
	public ImageView getIwSortie() {
		return iwSortie;
	}

	/**
	 * Met � jour l'ImageView de la Sortie de ControleurEditeur
	 * 
	 * @param iwSortie
	 * 
	 * @see ControleurEditeur#iwSortie
	 */
	public void setIwSortie(ImageView iwSortie) {
		this.iwSortie = iwSortie;
	}

	/**
	 * Retourne le bool�en qui permet de savoir si l'objet Sortie est s�l�ctionn� par l'utilisateur
	 * 
	 * @return un bool�en 
	 *
	 * @see ControleurEditeur#sortieSelected
	 */
		public boolean isSortieSelected() {
		return sortieSelected;
	}
		
	/**
	* Met � jour l'�tat du bool�en qui permet de savoir si l'utilisateur a s�l�ctionn� le bouton de Sortie
	* 
	* @param sortieSelected
	* 
	* @see ControleurEditeur#sortieSelected
	*/

	public void setSortieSelected(boolean sortieSelected) {
		this.sortieSelected = sortieSelected;
	}

	/**
	 * Retourne l'Objet du ControleurEditeur
	 * 
	 * @return une instance d'Objet
	 * 
	 * @see ControleurEditeur#obj
	 */
	public Objet getObj() {
		return obj;
	}

	/**
	 * Met � jour l'Objet du ControleurEditeur
	 * 
	 * @param obj
	 * 
	 * @see ControleurEditeur#obj
	 */
	public void setObj(Objet obj) {
		this.obj = obj;
	}

	/**
	 * Retourne l'Image � afficher du ControleurEditeur
	 * 
	 * @return l'instance de l'image d'un Objet
	 * 
	 * @see ControleurEditeur#imageChoisie
	 */
	public Image getImageChoisie() {
		return imageChoisie;
	}

	/**
	 * Met � jour l'Image � afficher du ControleurEditeur
	 * 
	 * @param imageChoisie
	 * 
	 * @see ControleurEditeur#imageChoisie
	 */
	public void setImageChoisie(Image imageChoisie) {
		this.imageChoisie = imageChoisie;
	}

	/**
	 * Initialize ControleurEditeur.
     * <p>
     * A la construction d'un objet ControleurEditeur,
     * On initialise toutes les cases de la GridPane avec une ImageView dont l'image est Vide.
     * Puis on initialise une m�thode pour chaque ImageView :
     * Cette m�thode est appel� lorsque l'on clique sur une des ImageView.
     * Elle permet de modifier l'imageview sur laquelle on clique avec l'Image s�l�ctionn� par l'utilisateur.
     * Et ainsi de cr�er une grille et de la modifier a chaque fois que l'on modifie une ImageView.
     * </p>
	 * 
	 * @throws FileNotFoundException
	 * 
	 * @see ControleurEditeur#grille
	 * @see ControleurEditeur#iwRockford
	 * @see ControleurEditeur#iwSortie
	 * @see ControleurEditeur#player
	 * @see ControleurEditeur#posSortie
	 * @see ControleurEditeur#imageChoisie
	 * @see ControleurEditeur#obj
	 * @see ControleurEditeur#_grid
	 */
	@FXML
	private void initialize() throws FileNotFoundException {
		Image imgVide = null;
		try {
			imgVide = new Image(new FileInputStream("@../../ressources/images/Theme1/Vide.png"));
		} catch (FileNotFoundException e) {
			erreur(AlertType.ERROR, "Fichier introuable",
					"Le fichier Vide.png est introuvable.\n");
		}
		for (int i = 0; i < Grille.HAUTEUR; i++) {
			for (int j = 0; j < Grille.LARGEUR; j++) {
				ImageView imageView = new ImageView();
				imageView.setImage(imgVide);
				imageView.setFitHeight(70);
				imageView.setFitWidth(70);
				int x = i;
				int y = j;
				imageView.setOnMouseClicked(new EventHandler<MouseEvent>() {
		            public void handle(MouseEvent e) {
		            	Image imgVide = null;
		        		try {
		        			imgVide = new Image(new FileInputStream("@../../ressources/images/Theme1/Vide.png"));
		        		} catch (FileNotFoundException e1) {
		        			erreur(AlertType.ERROR, "Fichier introuable",
		        					"Le fichier Vide.png est introuvable.\n");
		        		}
		                if (imageChoisie != null) {
		                	if (isRockfordSelected()) { // Rockford est s�lectionn�
		                		if (getIwRockford() == null) { // Aucun Rockford sur la map
		                			setIwRockford(imageView);
		                			imageView.setImage(imageChoisie);
		                			player = new Rockford(x, y);
									grille.setObjet(x, y, player);
									grille.setJoueur(player);
		                		} else { // Il y a d�j� un Rockford
									iwRockford.setImage(imgVide);
		                			imageView.setImage(imageChoisie);
		                			setIwRockford(imageView);
		                			grille.setObjet(player.getX(), player.getY(), new Vide());
		                			player = new Rockford(x, y);
									grille.setObjet(x, y, player);
									grille.setJoueur(player);
		                		}
		                		if (imageView.equals(getIwSortie())) { // Cas remplacement de la Sortie
			                		setIwSortie(null);
			                	}
		                	} else if (isSortieSelected()) { // Sortie s�lectionn�e
		                		if (getIwSortie() == null) { // Pas de sortie sur la map
		                			setIwSortie(imageView);
		                			imageView.setImage(imageChoisie);
		                			posSortie[0] = x; posSortie[1] = y;
									grille.setObjet(x, y, new Sortie());
		                		} else { // Il y a d�j� une sortie
									iwSortie.setImage(imgVide);
		                			imageView.setImage(imageChoisie);
		                			setIwSortie(imageView);
		                			grille.setObjet(posSortie[0], posSortie[1], new Vide());
		                			posSortie[0] = x; posSortie[1] = y;
									grille.setObjet(x, y, new Sortie());
		                		}
		                		if (imageView.equals(getIwRockford())) { // Cas remplacement de Rockford
		                			setIwRockford(null);
		                			grille.setJoueur(null);
		                		}
		                	} else { // Cas g�n�ral
		                		if (imageView.equals(getIwRockford())) { // Cas remplacement de Rockford
		                			setIwRockford(null);
		                			grille.setJoueur(null);
		                		}
		                		if (imageView.equals(getIwSortie())) { // Cas remplacement de la Sortie
			                		setIwSortie(null);
			                	}
		        				grille.setObjet(x, y, obj);
		                		imageView.setImage(imageChoisie);
		                	}
		                }
		            }
		        });
				grille.setObjet(i, j, new Vide());
				_grid.add(imageView, i, j);
				GridPane.setHalignment(imageView, HPos.CENTER);
				GridPane.setValignment(imageView, VPos.CENTER);
			}
		}
	}
	
	/**
	 * <p>
	 * Fonction appel� lorsque l'utilisateur appuie sur un des boutons, 
	 * elle permet de conna�tre l'image s�l�ctionn�e par l'utilisateur afin modifier la grille avec cette image.
	 * </p>
	 * 
	 * @param event
	 * 
	 * @see ControleurEditeur#setImageChoisie(Image)
	 * @see ControleurEditeur#setRockfordSelected(boolean)
	 * @see ControleurEditeur#setSortieSelected(boolean)
	 * @see ControleurEditeur#setObj(Objet)
	 * 
	 * @see ControleurEditeur#_boutonAcier
	 * @see ControleurEditeur#_boutonDiamant
	 * @see ControleurEditeur#_boutonMonstre
	 * @see ControleurEditeur#_boutonRocher
	 * @see ControleurEditeur#_boutonRockFord
	 * @see ControleurEditeur#_boutonTerre
	 * @see ControleurEditeur#_boutonSortie
	 * @see ControleurEditeur#_boutonVide
	 */
	@FXML
	public void choisirImage(ActionEvent event) {
		setRockfordSelected(false);
		setSortieSelected(false);
		Button bclic = (Button)(event.getSource());
		ImageView ivChoisie = (ImageView)(bclic.getChildrenUnmodifiable().get(0));
		setImageChoisie(ivChoisie.getImage());
		if (bclic.equals(_boutonRockFord)) {
			setRockfordSelected(true);
		} else if (bclic.equals(_boutonSortie)) {
			setSortieSelected(true);
		} else if (bclic.equals(_boutonVide)) {
			setObj(new Vide());
		} else if (bclic.equals(_boutonTerre)) {
			setObj(new Terre());
		} else if (bclic.equals(_boutonRocher)) {
			setObj(new Rocher());
		} else if (bclic.equals(_boutonMonstre)) {
			setObj(new Monstre());
		} else if (bclic.equals(_boutonDiamant)) {
			setObj(new Diamant());
		} else if (bclic.equals(_boutonAcier)) {
			setObj(new Acier());
		}
	}
	
	/**
	 * <p>
	 * Fonction appel� lorsque l'utilisateur souhaite sauvegarder la carte de jeu qu'il a cr��,
	 * Elle permet de sauvegarder la grille cr�� par l'utilisateur dans un fichier txt, soit cr�� soit existant !
	 * </p>
	 * 
	 * @param event
	 * 
	 * @see ControleurEditeur#grille
	 * 
	 * @see ControleurEditeur#getIwRockford()
	 * @see ControleurEditeur#erreur(AlertType, String, String)
	 * 
	 * @see Grille#getHauteur()
	 * @see Grille#getLargeur()
	 * @see Grille#getObjet(int, int)
	 */
	@FXML
	public void valider(ActionEvent event) {
		if (getIwRockford() == null) {
			Alert valider = erreur(AlertType.WARNING, "Avertissement", "Aucun Rockford n'est plac� sur la map !");
			valider.showAndWait();
			return;
		}
		try {
			FileChooser fileChooser = new FileChooser();
			fileChooser.setTitle("Choisissez votre carte");
			fileChooser.setInitialDirectory(new File("ressources/map"));
			fileChooser.getExtensionFilters().addAll(
					new FileChooser.ExtensionFilter("BoulderDash", "*.boulderdash"), 
					new FileChooser.ExtensionFilter("txt", "*.txt"));
		    Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
		    File file = fileChooser.showSaveDialog(stage);
		    if (file != null) {
		    	PrintWriter printwriter = new PrintWriter(file);
				printwriter.write(grille.getHauteur() + " " + grille.getLargeur() + "\n");
				for (int i = 0; i < grille.getHauteur(); i++) {
					for (int j = 0; j < grille.getLargeur(); j++) {
						printwriter.write(grille.getObjet(j, i).toString());
					}
					printwriter.println("");
				}
				printwriter.close(); 
		    }
			
		} catch (IOException e) {
			erreur(AlertType.ERROR, "Erreur", "Erreur : Probl�me lors de la sauvegarde du fichier. Abandon.");
			return;
		}
		Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
		window.close();
	}
	
	/**
	 * <p>
	 * Fonction appel� lorsque l'utilisateur souhaite quitter l'�diteur de niveaux sans sauvegarder,
	 * Elle permet de quitter la fenetre une fois que l'utilisateur � valider son choix.
	 * </p>
	 * 
	 * @param event
	 * 
	 * @see ControleurEditeur#erreur(AlertType, String, String)
	 */
	@FXML
	public void quitter(ActionEvent event) {
		Alert quitter = erreur(AlertType.CONFIRMATION, "Attention", "Voulez-vous vraiment quitter sans sauvegarder ?");
		quitter.getButtonTypes().add(ButtonType.CANCEL);
		Optional<ButtonType> reponse= quitter.showAndWait();
		if (reponse.get() == ButtonType.OK) {  
			Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
			window.close();
		}
	}
	
	/**
	 * Retourne une boite de dialogue dont le titre, le texte et le type d'Alerte sont pass�s en param�tres.
	 * 
	 * @param alert
	 * @param title
	 * @param message
	 * 
	 * @return Une boite de dialog de type Alert
	 */
	public Alert erreur(AlertType alert, String title, String message) {
		Alert dialogA = new Alert(alert);
		dialogA.getButtonTypes().setAll(ButtonType.OK);
		dialogA.setHeaderText(null);
		dialogA.setTitle(title);
		dialogA.setContentText(message);
		return dialogA;
	}
	
}
