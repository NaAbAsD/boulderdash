package application;

import java.net.URL;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

public class FenetrePrincipale extends Application {

	@Override
	public void start(Stage primaryStage) throws Exception {
		try {
			FXMLLoader loader = new FXMLLoader(new URL("file:" + System.getProperty("user.dir") + "/ressources/fxml/FenetreAccueil.fxml"));
			ControleurFenetreAccueil cont_Boite = new ControleurFenetreAccueil(primaryStage);
			loader.setController(cont_Boite);
			Pane root = loader.load();
			Scene scene = new Scene(root, 1265, 705);
			primaryStage.setScene(scene);
			primaryStage.setTitle("Boulderdash");
			primaryStage.setResizable(false);
			primaryStage.show();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		launch(args);	
	}
	
}