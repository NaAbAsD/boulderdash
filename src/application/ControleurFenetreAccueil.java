package application;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.Optional;

import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.ListView;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.image.Image;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.util.Callback;
import modele.cor.detecteurs.DetecteurDiamantRockford;
import modele.cor.detecteurs.DetecteurMonstreRockford;
import modele.cor.detecteurs.DetecteurRocherMonstre;
import modele.cor.detecteurs.DetecteurRocherRocherChute;
import modele.cor.detecteurs.DetecteurRocherRockford;
import modele.cor.detecteurs.DetecteurRockfordDiamant;
import modele.cor.detecteurs.DetecteurRockfordMonstre;
import modele.cor.detecteurs.DetecteurRockfordRocher;
import modele.cor.detecteurs.DetecteurRockfordSortie;
import modele.cor.detecteurs.DetecteurRockfordTerre;
import modele.cor.detecteurs.DetecteurVide;
import modele.exceptions.BoulderException;
import modele.exceptions.EtatJeuException;
import modele.plateau.Grille;
import modele.plateau.Plateau;
import modele.plateau.moteur.Deplacement;
import modele.plateau.moteur.MoteurDeplacementAuto;
import modele.plateau.moteur.MoteurGravite;
import modele.plateau.objets.Acier;
import modele.plateau.objets.Diamant;
import modele.plateau.objets.Monstre;
import modele.plateau.objets.Objet;
import modele.plateau.objets.Rocher;
import modele.plateau.objets.Rockford;
import modele.plateau.objets.Sortie;
import modele.plateau.objets.Terre;
import modele.plateau.objets.Vide;
import ui.PanneauFooter;
import ui.gestion.Timer;

/**
 * <b>ControleurFenetreAccueil est la classe repr�sentant le controleur de la fenetre d'accueil du Boulderdash.</b>
 * <p>
 * Ce controleur est caract�ris� par les informations suivantes :
 * <ul>
 * <li>Le stage principal � envoyer aux autres contr�leurs afin de la modifier.</li>
 * <li>Le canvas o� se d�roule le jeu.</li>
 * <li>Une borderpane contenant le canvas cit� au dessus.</li>
 * <li>La scene o� le constructeur prend la borderpane en param�tre.</li>
 * <li>Une instance de panneaufooter afin de modifier le timer, le nombre de vies et de diamants.</li>
 * <li>Une Hashmap d'Image afin d'ajouter les images de chaque entit�.</li>
 * <li>Une instance de Plateau permettant de faire jouer l'utilisateur dessus.</li>
 * <li>Une instance de Grille permettant de mettre � jour et d'utiliser la map de jeu.</li>
 * <li>Une instance de MoteurGravite permettant de modifier le Timer de timerGravite</li>
 * <li>Une instance de MoteurDeplacementAuto permettant de modifier le Timer de timerDeplacementAuto.</li>
 * <li>Une instance de Timer permettant de modifier le Timer de la gravit�</li>
 * <li>Une instance de Timer permettant de modifier le Timer du d�placement auto des monstres.</li>
 * <li>Trois boutons d�finis dans un fichier FXML, ces boutons servent � choisir l'action du menu � faire.</li>
 * </ul>
 * </p>
 * 
 * @see PanneauFooter
 * @see Plateau
 * @see Grille
 * @see MoteurGravite
 * @see MoteurDeplacementAuto
 * @see Timer
 */
public class ControleurFenetreAccueil {
	
	/**
	 * Le stage principal � envoyer aux autres contr�leurs afin de la modifier.
	 * 
	 * @see ControleurFenetreAccueil#getPrimaryStage()
	 * @see ControleurFenetreAccueil#setPrimaryStage(Stage)
	 */
	private Stage primaryStage;
	
	/**
	 * Le canvas o� se d�roule le jeu.
	 */
	private Canvas grillePane;
	
	/**
	 * Une borderpane contenant le canvas cit� au dessus.
	 */
	private BorderPane root;
	
	/**
	 * La scene o� le constructeur prend la borderpane en param�tre.
	 */
	private Scene scene;
	
	/**
	 * Une instance de panneaufooter afin de modifier le timer, le nombre de vies et de diamants.
	 * 
	 * @see ControleurFenetreAccueil#getPanneauFooter()
	 * @see ControleurFenetreAccueil#setPanneauFooter(PanneauFooter)
	 */
	private PanneauFooter panneauFooter;
	
	/**
	 * Une Hashmap d'Image afin d'ajouter les images de chaque entit�.
	 */
	private HashMap<Class<?>, Image> tabImage;
	
	/**
	 * Une instance de Plateau permettant de faire jouer l'utilisateur dessus.
	 */
	private Plateau plateau;
	
	/**
	 * Une instance de Grille permettant de mettre � jour et d'utiliser la map de jeu.
	 */
	private Grille grille;
	
	/**
	 * Une instance de MoteurGravite permettant de modifier le Timer de timerGravite.
	 */
	private MoteurGravite moteurGravite;
	
	/**
	 * Une instance de MoteurDeplacementAuto permettant de modifier le Timer de timerDeplacementAuto.
	 */
	private MoteurDeplacementAuto moteurDeplacementAuto;
	
	/**
	 * Une instance de Timer permettant de modifier le Timer de la gravit�
	 */
	private Timer timerGravite;
	
	/**
	 * Une instance de Timer permettant de modifier le Timer du d�placement auto des monstres.
	 */
	private Timer timerDeplacementAuto;
	
	/**
	 * Trois boutons d�finis dans un fichier FXML, ces boutons servent � choisir l'action du menu � faire.
	 */
	@FXML private Button _boutonJouer;
	@FXML private Button _boutonAide;
	@FXML private Button _boutonEdit;
	
	/**
	 * Constructeur de ControleurFenetreAccueil
	 * 
	 * @param primaryStage
	 * 			Le Stage � modifier.
	 * 
	 * @see ControleurFenetreAccueil#setPrimaryStage(Stage)
	 */
	public ControleurFenetreAccueil(Stage primaryStage) {
		setPrimaryStage(primaryStage);
	}

	/**
	 * Retourne l'instance de Stage utilis� par ce Controleur.
	 * 
	 * @return la Stage du Controleur
	 * 
	 * @see ControleurFenetreAccueil#primaryStage
	 */
	public Stage getPrimaryStage() {
		return primaryStage;
	}

	/**
	 * Met � jour l'instance de la Stage du jeu.
	 * 
	 * @param primaryStage
	 * 
	 * @see ControleurFenetreAccueil#primaryStage
	 */
	public void setPrimaryStage(Stage primaryStage) {
		this.primaryStage = primaryStage;
	}
	
	/**
	 * Retourne le PanneauFooter affich� lors de la partie.
	 * 
	 * @return l'instance du PanneauFooter utilis� par ce Controleur.
	 * 
	 * @see PanneauFooter
	 * 
	 * @see ControleurFenetreAccueil#panneauFooter
	 */
	public PanneauFooter getPanneauFooter() {
		return panneauFooter;
	}

	/**
	 * Met � jour le PanneauFooter affich� lors de partie.
	 * 
	 * @param panneauFooter
	 * 
	 * @see PanneauFooter
	 * 
	 * @see ControleurFenetreAccueil#panneauFooter
	 */
	public void setPanneauFooter(PanneauFooter panneauFooter) {
		this.panneauFooter = panneauFooter;
	}

	/**
	 * Initialize ControleurFenetreAccueil.
     * <p>
     * A la construction d'un objet ControleurFenetreAccueil,
     * On modifie la couleur du fond des boutons du fichier FXML.
     * </p>
	 * 
	 * @see ControleurFenetreAccueil#_boutonJouer
	 * @see ControleurFenetreAccueil#_boutonEdit
	 * @see ControleurFenetreAccueil#_boutonAide
	 */
	@FXML
	private void initialize() {
		_boutonJouer.setBackground(new Background(new BackgroundFill(Color.BLACK, CornerRadii.EMPTY, Insets.EMPTY)));
		_boutonEdit.setBackground(new Background(new BackgroundFill(Color.BLACK, CornerRadii.EMPTY, Insets.EMPTY)));
		_boutonAide.setBackground(new Background(new BackgroundFill(Color.BLACK, CornerRadii.EMPTY, Insets.EMPTY)));
	}
	
	/**
	 * <p>La fonction jouer est appel� lorsque l'utilisateur clique sur le bouton jouer du menu.</p>
	 * 
	 * @throws Exception
	 * 
	 * @see ControleurFenetreAccueil#plateau
	 * @see ControleurFenetreAccueil#grille
	 * @see ControleurFenetreAccueil#root
	 * @see ControleurFenetreAccueil#scene
	 * @see ControleurFenetreAccueil#primaryStage
	 * 
	 * @see ControleurFenetreAccueil#initFooter()
	 * @see ControleurFenetreAccueil#initGrille()
	 * @see ControleurFenetreAccueil#initImages(int)
	 * @see ControleurFenetreAccueil#dessinerGrille()
	 * @see ControleurFenetreAccueil#genererMoteurPhysique()
	 * 
	 * @see Plateau
	 * @see Plateau#getGrille()
	 * @see Plateau#ajouterDetecteur(modele.cor.detecteurs.DetecteurInteractions)
	 */
	@FXML
	public void jouer() throws Exception {
		plateau = new Plateau();
		grille = plateau.getGrille();
		plateau.ajouterDetecteur(new DetecteurVide());
        plateau.ajouterDetecteur(new DetecteurRockfordMonstre());
        plateau.ajouterDetecteur(new DetecteurRockfordTerre());
        plateau.ajouterDetecteur(new DetecteurRockfordDiamant());
        plateau.ajouterDetecteur(new DetecteurRockfordRocher());
        plateau.ajouterDetecteur(new DetecteurRockfordSortie());
        plateau.ajouterDetecteur(new DetecteurRocherRockford());
        plateau.ajouterDetecteur(new DetecteurRocherMonstre());
        plateau.ajouterDetecteur(new DetecteurRocherRocherChute());
        plateau.ajouterDetecteur(new DetecteurDiamantRockford());
        plateau.ajouterDetecteur(new DetecteurMonstreRockford());
        
		root = new BorderPane(grillePane);
		scene = new Scene(root);
		scene.setOnKeyPressed(new HandlerClavier());
		
		initImages(1);
		try {
			File map = new File("ressources/map/niveaux/niveau-0.boulderdash");
			grille.readGrilleFromFile(map);
		} catch (Exception e) {
			throw e;
		}
		initGrille();
		initFooter();
		dessinerGrille();
		genererMoteurPhysique();
		
		primaryStage.setScene(scene);
		primaryStage.show();
	}
	
	/**
	  * <p>
	  * editeurNiveau est appel�e lorsque l'on clique sur le bouton editeur de la page d'accueil.
	  * Cette m�thode ouvre une nouvelle fen�tre afin de pouvoir cr�er un niveau.
	  * Le contr�leur de la fen�tre est modifi� pour ControleurEditeur.
	  * </p>
	  * 
	  * @see ControleurEditeur
	  * 
	  * @see ControleurFenetreAccueil#erreur(AlertType, String, String)
	  */
	@FXML
	private void editeurNiveau() {
		try {
			FXMLLoader loader = new FXMLLoader(new URL("file:" + System.getProperty("user.dir") + "/ressources/fxml/FenetreEditor.fxml"));
			ControleurEditeur cont_Edit = new ControleurEditeur();
			loader.setController(cont_Edit);
	        Stage stage = new Stage();
	        HBox root = loader.load();
	        root.setBackground(new Background(new BackgroundFill(Color.WHITE, CornerRadii.EMPTY, Insets.EMPTY)));
	        root.requestFocus();
	        Scene scene = new Scene(root, 1050, 810);
	        stage.setScene(scene);
	        stage.setTitle("BoulderDash - Editeur de niveaux");
	        stage.setResizable(false);
	        stage.show();
		} catch (IOException e) {
			erreur(AlertType.ERROR, "Fichier Introuvable", "Une ou plusieurs images ressources sont introuvables.\n" + 
					"L'ex�cution est interrompue !");
			Platform.exit();
		}
	}
	
	/**
	  * <p>
	  * aide est appel�e lorsque l'on clique sur le bouton aide de la page d'accueil 
	  * ou sur le bouton H de son clavier lorsque la partie est lanc�e.
	  * Cette m�thode ouvre une boite de dialog de type Alert.
	  * </p>
	  * 
	  * @see ControleurFenetreAccueil#erreur(AlertType, String, String)
	  */
	@FXML
	private void aide() {
		erreur(AlertType.INFORMATION, "BoulderDash - Aide",
				"Diff�rents raccourcis sont disponibles au cours de votre partie de Boulderdash :\n\n" +
				"  - La touche \"F5\" de votre clavier permet de charger une nouvelle map.\n" +
				"  - La touche \"R\" permet de red�marrer le jeu ! Attention !\n" +
				"  - La touche \"H\" lance la boite de dialogue d'aide.\n" +
				"  - La touche \"T\" permet de modifier le th�me.");
	}
	
	/**
	 * <p>La fonction initFooter intialise le footer de ControleurFenetreAccueil avec une nouvelle instance que l'on set en bas du borderpane.</p>
	 * 
	 * @see ControleurFenetreAccueil#panneauFooter
	 * @see ControleurFenetreAccueil#plateau
	 * @see ControleurFenetreAccueil#root
	 */
	private void initFooter() {
		try {
			panneauFooter = new PanneauFooter(plateau);
		} catch (BoulderException e) {
			System.out.println(e.getMessage());
		}
		((BorderPane)root).setBottom(panneauFooter);
		BorderPane.setAlignment(panneauFooter, Pos.CENTER);
	}

	/**
	 * <p>La fonction initImages intialise la HashMap de ControleurFenetreAccueil 
	 * avec l'Image de chaque image utile aux entiti�s du jeu.</p>
	 * 
	 * @see ControleurFenetreAccueil#tabImage
	 * 
	 * @see ControleurFenetreAccueil#erreur(AlertType, String, String)
	 */
	private void initImages(int numTheme) {
		tabImage = new HashMap<Class<?>, Image>();
		Image image;
		String num = Integer.toString(numTheme);
		try {
			image = new Image(new FileInputStream("@../../ressources/images/Theme" + num + "/Vide.png"));
			tabImage.put(Vide.class, image);
			image = new Image(new FileInputStream("@../../ressources/images/Theme" + num + "/Rockford.png"));
			tabImage.put(Rockford.class, image);
			image = new Image(new FileInputStream("@../../ressources/images/Theme" + num + "/Rocher.png"));
			tabImage.put(Rocher.class, image);
			image = new Image(new FileInputStream("@../../ressources/images/Theme" + num + "/Acier.png"));
			tabImage.put(Acier.class, image);
			image = new Image(new FileInputStream("@../../ressources/images/Theme" + num + "/Diamant.png"));
			tabImage.put(Diamant.class, image);
			image = new Image(new FileInputStream("@../../ressources/images/Theme" + num + "/Monstre.png"));
			tabImage.put(Monstre.class, image);
			image = new Image(new FileInputStream("@../../ressources/images/Theme" + num + "/Terre.png"));
			tabImage.put(Terre.class, image);
			image = new Image(new FileInputStream("@../../ressources/images/Theme" + num + "/Sortie.png"));
			tabImage.put(Sortie.class, image);
		} catch (FileNotFoundException e) {
			erreur(AlertType.ERROR, "Fichier Introuvable", "Une ou plusieurs images ressources sont introuvables.\n" + 
					"L'ex�cution est interrompue !");
			Platform.exit();
		}
	}
	
	/**
	 * <p>La fonction initGrille intialise la Grille de ControleurFenetreAccueil 
	 * avec une nouvelle instance o� l'on set la hauteur et largeur du canvas du jeu.</p>
	 * 
	 * @see ControleurFenetreAccueil#grille
	 * @see ControleurFenetreAccueil#grillePane
	 * @see ControleurFenetreAccueil#root
	 */
	private void initGrille() {
		int lGrille = 64 * grille.getHauteur();
		int hGrille = 64 * grille.getLargeur();
		grillePane = new Canvas(lGrille, hGrille);
		((BorderPane)root).setCenter(grillePane);
		grillePane.getGraphicsContext2D();
	}

	/**
	 * <p> La fonction dessinerGrille r�cup�re la grille de ControleurFenetreAccueil afin d'obtenir les objets de la grille 
	 * et d'ainsi afficher l'image correspondante � l'Objet.</p>
	 * 
	 * @see Grille
	 * @see Grille#getHauteur()
	 * @see Grille#getLargeur()
	 * 
	 * @see ControleurFenetreAccueil#grille
	 * @see ControleurFenetreAccueil#tabImage
	 * @see ControleurFenetreAccueil#grillePane
	 */
	public void dessinerGrille() {
		for (int l = 0; l < grille.getHauteur(); l++) {
			for (int c = 0; c < grille.getLargeur(); c++) {
				Objet objet = grille.getObjet(l, c);
				Image image = tabImage.get(objet.getClass());
				grillePane.getGraphicsContext2D().drawImage(image, c * 64, l * 64);
			}
		}
	}
	
	public void genererMoteurPhysique() throws BoulderException {
		// Gestion anciens moteurs
		if (timerGravite != null) {
			timerGravite.getTimeline().stop();
		}
		if (timerDeplacementAuto != null) {
			timerDeplacementAuto.getTimeline().stop();
		}
		try {
			// Gestion gravit�
			moteurGravite = new MoteurGravite(this, plateau);
			timerGravite = new Timer(0.15);
			timerGravite.add(moteurGravite);
			// Gestion d�placement auto
			moteurDeplacementAuto = new MoteurDeplacementAuto(this, plateau);
			timerDeplacementAuto = new Timer(0.4);
			timerDeplacementAuto.add(moteurDeplacementAuto);
		} catch (BoulderException e) {
			throw e;
		}
	}
	
	/**
	 * <p>Fonction permettant de choisir un fichier pour jouer.
	 * Si le fichier est du type niveau alors on modifie la valeur bool�ene du NiveauCustom du Plateau</p>
	 * 
	 * @throws Exception
	 * 
	 * @see Plateau
	 * @see Plateau#setNiveauCustom(boolean)
	 * 
	 * @see Grille
	 * @see Grille#readGrilleFromFile(File)
	 * 
	 * @see ControleurFenetreAccueil#grille
	 * @see ControleurFenetreAccueil#plateau
	 */
	public void openFileChooser() throws Exception {
		FileChooser fileChooser = new FileChooser();
		Stage stage = new Stage();
		fileChooser.setInitialDirectory(new File("ressources/map"));
        File file = fileChooser.showOpenDialog(stage);
        if (file != null) {
        	if (!file.getAbsolutePath().contains("\\niveaux\\niveau-")) {
        		plateau.setNiveauCustom(true);
        	}
        	try {
				grille.readGrilleFromFile(file);
			} catch (Exception e) {
				throw e;
			}
        }
	}
	
	/**
     * <p>Fonction permettant de changer la fenetre principale du jeu pour la fenetre de menu, lors d'une defaite ou d'une victoire</p>
     * 
     * @see ControleurFenetreAccueil#primaryStage
     * 
     * @see ControleurFenetreAccueil#erreur(AlertType, String, String)
     */
	public void loadMenu() {
		try {
			FXMLLoader loader = new FXMLLoader(new URL("file:" + System.getProperty("user.dir") + "/ressources/fxml/FenetreAccueil.fxml"));
			ControleurFenetreAccueil cont_Boite = new ControleurFenetreAccueil(primaryStage);
			loader.setController(cont_Boite);
			Pane root = loader.load();
			Scene scene = new Scene(root, 1265, 705);
			primaryStage.setScene(scene);
			primaryStage.setTitle("Boulderdash");
			primaryStage.setResizable(false);
			primaryStage.show();
		} catch (IOException e) {
			erreur(AlertType.ERROR, "Fichier Introuvable", "Le fichier FenetreAccueil.fxml est introuvable.\n" + 
					"L'ex�cution est interrompue !");
			Platform.exit();
		}
	}

	/**
	 * <p>Methode appel� lorsque l'utilisateur clique sur un bouton de son clavier.
	 * On verifie d'abord que la touche saisie soit valide. Si elle l'est on applique alors ce qu'elle doit faire
	 * <li>UP : Monte Rockford sur la map</li>
	 * <li>DOWN : Descend Rockford sur la map</li>
	 * <li>RIGHT : Rockford se d�place � droite</li>
	 * <li>LEFT : Rockford se d�place � gauche</li>
	 * <li>R : permet � l'utilisateur de rejouer</li>
	 * <li>F5 : permet � l'utilisateur de modifier la Grille/la map</li>
	 * <li>H : permet � l'utilisateur d'afficher une boite d'aide</li>
	 * <li>T : permet � l'utilisateur de modifier le th�me du jeu</li>
	 * </p>
	 * 
	 * @see Rockford
	 * @see Rockford#getX()
	 * @see Rockford#getY()
	 * @see Rockford#deplacementBas()
	 * @see Rockford#deplacementGauche()
	 * @see Rockford#deplacementDroite()
	 * @see Rockford#deplacementHaut()
	 * 
	 * @see Grille
	 * @see Grille#getJoueur()
	 * 
	 * @see Plateau
	 * @see Plateau#jouer(Deplacement)
	 * 
	 * @see Timer
	 * @see Timer#getTimeline()
	 * 
	 * @see PanneauFooter
	 * @see PanneauFooter#getTimer()
	 * 
	 * @see ControleurListeTheme
	 * @see ControleurListeTheme#valider()
	 * @see ControleurListeTheme#quitter()
	 * 
	 * @see ControleurFenetreAccueil#grille
	 * @see ControleurFenetreAccueil#plateau
	 * @see ControleurFenetreAccueil#timerGravite
	 * @see ControleurFenetreAccueil#timerDeplacementAuto
	 * @see ControleurFenetreAccueil#panneauFooter
	 * 
	 * @see EtatJeuException
	 * @see EtatJeuException#getEtat()
	 * @see EtatJeuException#VICTOIRE
	 * @see EtatJeuException#DEFAITE
	 * 
	 * @see ControleurFenetreAccueil#openFileChooser()
	 * @see ControleurFenetreAccueil#initFooter()
	 * @see ControleurFenetreAccueil#initGrille()
	 * @see ControleurFenetreAccueil#initImages(int)
	 * @see ControleurFenetreAccueil#dessinerGrille()
	 * @see ControleurFenetreAccueil#genererMoteurPhysique()
	 * @see ControleurFenetreAccueil#aide()
	 * @see ControleurFenetreAccueil#erreur(AlertType, String, String)
	 * @see ControleurFenetreAccueil#jouer()
	 * 
	 */
	private final class HandlerClavier implements EventHandler<KeyEvent> {
		public void handle(KeyEvent ke) {
			Rockford joueur = grille.getJoueur();
			try {
				switch (ke.getCode()) {
					case UP: {
						if (joueur.getX() != 0) {
							plateau.jouer(new Deplacement(plateau, joueur, joueur.getX(), joueur.getY(), Deplacement.HAUT));
							if (!(grille.getObjet(joueur.getX(), joueur.getY()) instanceof Rockford)) { // Si on ne se d�place pas
								joueur.deplacementHaut();
							}
						}
						break;
					}
					case DOWN: {
						if (joueur.getX() != grille.getHauteur() - 1) {
							plateau.jouer(new Deplacement(plateau, joueur, joueur.getX(), joueur.getY(), Deplacement.BAS));
							if (!(grille.getObjet(joueur.getX(), joueur.getY()) instanceof Rockford)) {
								joueur.deplacementBas();
							}
						}
						break;
					}
					case RIGHT: {
						if (joueur.getY() != grille.getLargeur() - 1) {
							plateau.jouer(new Deplacement(plateau, joueur, joueur.getX(), joueur.getY(), Deplacement.DROITE));
							if (!(grille.getObjet(joueur.getX(), joueur.getY()) instanceof Rockford)) {
								joueur.deplacementDroite();
							}
						}
						break;
					}
					case LEFT: {
						if (joueur.getY() != 0) {
							plateau.jouer(new Deplacement(plateau, joueur, joueur.getX(), joueur.getY(), Deplacement.GAUCHE));
							if (!(grille.getObjet(joueur.getX(), joueur.getY()) instanceof Rockford)) {
								joueur.deplacementGauche();
							}
						}
						break;
					}
					case F5: { // Changement de carte
						openFileChooser();
						initFooter();
						initGrille();
						dessinerGrille();
						genererMoteurPhysique();
						return;
					}
					case H: { // Affichage de l'aide
						timerGravite.getTimeline().pause();
						timerDeplacementAuto.getTimeline().pause();
						panneauFooter.getTimer().getTimeline().pause();
						aide();
						timerGravite.getTimeline().play();
						timerDeplacementAuto.getTimeline().play();
						panneauFooter.getTimer().getTimeline().play();
						break;
					}
					case T: { // Changement de th�me
						Dialog<Integer> dialog = new Dialog<>();
						dialog.getDialogPane().setBackground(new Background(new BackgroundFill(Color.WHITE, CornerRadii.EMPTY, Insets.EMPTY)));			 
						FXMLLoader loader = new FXMLLoader(new URL("file:" + System.getProperty("user.dir") + "/ressources/fxml/FenetreListeTheme.fxml"));
						ControleurListeTheme cont_Theme = new ControleurListeTheme();
						loader.setController(cont_Theme);
						ListView<String> list = loader.load();
						
						dialog.getDialogPane().setContent(list);
						dialog.setTitle("BoulderDash - S�l�ction th�me");
						ButtonType buttonTypeValider = new ButtonType("Valider", ButtonData.OK_DONE);
						ButtonType buttonTypeAnnuler = new ButtonType("Annuler", ButtonData.CANCEL_CLOSE);
						dialog.getDialogPane().getButtonTypes().addAll(buttonTypeValider,  buttonTypeAnnuler);
						 
						dialog.setResultConverter(new Callback<ButtonType, Integer>() {
						    @Override
						    public Integer call(ButtonType b) {
						 
						        if (b == buttonTypeAnnuler) {
						            return cont_Theme.quitter();
						        }else{
						        	return cont_Theme.valider();
						        }
						    }
						});
						Optional<Integer> result = dialog.showAndWait();
						if (result.get() != -1) {
							initImages(result.get());
							dessinerGrille();
						}
						break;
					}
					case R: { // Red�marrage du jeu
						jouer();
						return;
					}
					default: { // Autre bouton
						return;
					}
				}
				dessinerGrille();
			} catch (EtatJeuException etat) {
				if (etat.getEtat() == EtatJeuException.VICTOIRE) {
					erreur(AlertType.INFORMATION, "Victoire !", "Vous avez gagn� ! (pour le moment)");
				} else {
					erreur(AlertType.INFORMATION, "Defaite !", "Vous avez perdu !");
				}
				loadMenu();
			} catch (Exception e) {
				erreur(AlertType.ERROR, "Erreur", e.getMessage());
			}
		}
	}
	
	/**
	 * Retourne une boite de dialogue dont le titre, le texte et le type d'Alerte sont pass�s en param�tres.
	 * 
	 * @param alert
	 * @param title
	 * @param message
	 * 
	 * @return Une boite de dialog de type Alert
	 */
	public void erreur(AlertType alert, String title, String message) {
		Alert dialogA = new Alert(alert);
		dialogA.getButtonTypes().setAll(ButtonType.OK);
		dialogA.setHeaderText(null);
		dialogA.setTitle(title);
		dialogA.setContentText(message);
		dialogA.show();
	}
	
}
