package modele.plateau.objets;

import modele.plateau.Plateau;

/**
 * <b>La classe Sortie permet de g�n�rer les Objets de grille de son type.</b>
 */
public class Sortie extends Objet {
	
	/**
	 * Constructeur de Sortie
	 */
	public Sortie() {
		super();
	}

	/**
	 * Gravit� non appliqu�e sur Sortie
	 * 
	 * @see Objet#gravite(Plateau, int, int)
	 */
	@Override
	public void gravite(Plateau plateau, int x, int y) {}

	/**
	 * Retourne null
	 * 
	 * @return null car la Sortie ne se d�place pas automatiquement
	 * 
	 * @see Objet
	 * @see Objet#seDeplace(Plateau, int, int)
	 */
	@Override
	public Objet seDeplace(Plateau plateau, int x, int y) {
		return null;
	}

	/**
	 * Retourne le String de Sortie
	 * 
	 * @see Objet#toString()
	 */
	@Override
	public String toString() {
		return "S";
	}

}
