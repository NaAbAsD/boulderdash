package modele.plateau.objets;

import modele.plateau.Plateau;
import modele.plateau.moteur.Deplacement;

/**
 * <b>La classe Rocher permet de g�n�rer les Objets de grille de son type.</b>
 */
public class Rocher extends Objet {
	
	/**
	 * Constructeur de Rocher
	 */
	public Rocher() {
		super();
	}
	
	/**
	 * Fonction appliquant la gravit� sur Rocher
	 * 
	 * @see Objet
	 * @see Objet#gravite(Plateau, int, int)
	 */
	@Override
	public void gravite(Plateau plateau, int x, int y) throws Exception {
		Deplacement dep = new Deplacement(plateau, this, x, y, Deplacement.BAS);
		try {
			if (dep.isValide() && plateau.jouer(dep)) {
				if (plateau.getGrille().getObjet(dep.getlArr(), dep.getcArr()) == this) { // Si on a r�ellement boug�
					setEnChute(true);
				} else {
					setEnChute(false);
				}
			} else {
				setEnChute(false);
			}
		} catch (Exception e) {
			throw e;
		}
	}
	
	/**
	 * Retourne null
	 * 
	 * @return null car le Rocher ne se d�place pas automatiquement
	 * 
	 * @see Objet
	 * @see Objet#seDeplace(Plateau, int, int)
	 */
	@Override
	public Objet seDeplace(Plateau plateau, int x, int y) {
		return null;
	}
	
	/**
	 * Retourne le String de Rocher
	 * 
	 * @see Objet#toString()
	 */
	@Override
	public String toString() {
		return "R";
	}

}
