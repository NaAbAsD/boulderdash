package modele.plateau.objets;

import java.util.ArrayList;
import java.util.Arrays;

import modele.plateau.Plateau;
import modele.plateau.moteur.Deplacement;

/**
 * <b>La classe Monstre permet de g�n�rer les Objets de grille de son type.</b>
 */
public class Monstre extends Objet {

	/**
	 * Constructeur de Monstre
	 */
	public Monstre() {
		super();
	}
	
	/**
	 * Gravit� non appliqu�e sur Monstre
	 * 
	 * @see Objet#gravite(Plateau, int, int)
	 */
	@Override
	public void gravite(Plateau plateau, int x, int y) {}
	
	/**
	 * Retourne le Monstre apr�s avoir effectu� un d�placement al�atoire
	 * 
	 * @see Objet
	 * @see Objet#seDeplace(Plateau, int, int)
	 * @see Objet#randInt(int, int)
	 */
	public Objet seDeplace(Plateau plateau, int x, int y) throws Exception {
		int choix;
		Deplacement dep;
		ArrayList<Integer> depPossible = new ArrayList<Integer>(4);
		depPossible.addAll(Arrays.asList(Deplacement.HAUT, Deplacement.DROITE, Deplacement.BAS, Deplacement.GAUCHE));
		while (depPossible.size() != 0) { // Tant qu'il y a des possibilit�s de d�placement
			choix = randInt(0, depPossible.size());
			dep = new Deplacement(plateau, this, x, y, depPossible.get(choix));
			try {
				if (dep.isValide() && plateau.jouer(dep)) { // Si on peut se d�placer
					setEnMouvementAuto(true);
					return this;
				}
			} catch (Exception e) {
				throw e;
			}
			depPossible.remove(choix); // Sinon on retire cette possibilit� et on retente avec d'autres si disponible.
		}
		return null;
	}
	
	/**
	 * Retourne le String de Monstre
	 * 
	 * @see Objet#toString()
	 */
	@Override
	public String toString() {
		return "M";
	}

}
