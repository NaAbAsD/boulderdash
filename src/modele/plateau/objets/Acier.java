package modele.plateau.objets;

import modele.plateau.Plateau;

/**
 * <b>La classe Acier permet de g�n�rer les Objets de grille de son type.</b>
 */
public class Acier extends Objet {
	/**
	 * Constructeur de Acier
	 */
	public Acier() {
		super();
	}
	
	/**
	 * Gravit� non appliqu�e sur Acier
	 * 
	 * @see Objet#gravite(Plateau, int, int)
	 */
	@Override
	public void gravite(Plateau plateau, int x, int y) {}
	
	/**
	 * Retourne null
	 * 
	 * @return null car l'Acier ne se d�place pas automatiquement
	 * 
	 * @see Objet
	 * @see Objet#seDeplace(Plateau, int, int)
	 */
	@Override
	public Objet seDeplace(Plateau plateau, int x, int y) {
		return null;
	}

	/**
	 * Retourne le String de Acier
	 * 
	 * @see Objet#toString()
	 */
	@Override
	public String toString() {
		return "A";
	}
	
}
