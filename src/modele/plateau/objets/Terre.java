package modele.plateau.objets;

import modele.plateau.Plateau;

/**
 * <b>La classe Terre permet de g�n�rer les Objets de grille de son type.</b>
 */
public class Terre extends Objet {

	/**
	 * Constructeur de Terre
	 */
	public Terre() {
		super();
	}
	
	/**
	 * Gravit� non appliqu�e sur Terre
	 * 
	 * @see Objet#gravite(Plateau, int, int)
	 */
	@Override
	public void gravite(Plateau plateau, int x, int y) {}
	
	/**
	 * Retourne null
	 * 
	 * @return null car la Terre ne se d�place pas automatiquement
	 * 
	 * @see Objet
	 * @see Objet#seDeplace(Plateau, int, int)
	 */
	@Override
	public Objet seDeplace(Plateau plateau, int x, int y) {
		return null;
	}
	
	/**
	 * Retourne le String de Terre
	 * 
	 * @see Objet#toString()
	 */
	@Override
	public String toString() {
		return "T";
	}

}
