package modele.plateau.objets;

import java.util.Random;

import modele.plateau.Plateau;

/**
 * <b>Objet est la classe abstraite servant de mod�le � toutes les autres classes Objets.</b>
 * <p>
 * Cette classe est caract�ris�e par l'information suivante :
 * <ul>
 * <li>Un bool�en permettant de savoir si l'objet est en chute.</li>
 * <li>Un bool�en permettant de savoir si l'objet est en mouvement automatique.</li>
 * </ul>
 * </p>
 */
public abstract class Objet {
	
	/**
	 * Un bool�en servant � savoir si l'Objet est en chute.
	 * 
	 * @see Objet#isEnChute()
	 * @see Objet#setEnChute(boolean)
	 */
	private boolean enChute;
	
	/**
	 * Un bool�en servant � savoir si l'Objet est en mouvement automatique.
	 * 
	 * @see Objet#isEnMouvementAuto()
	 * @see Objet#setEnMouvementAuto(boolean)
	 */
	private boolean enMouvementAuto;
	
	/**
	 * Constructeur de l'Objet
	 */
	public Objet() {}

	/**
	 * Retourne le boolean permettant de savoir si l'obet est en chute.
	 * 
	 * @return un boolean.
	 * 
	 * @see Objet#enChute
	 */
	public boolean isEnChute() {
		return enChute;
	}

	/**
	 * Met � jour la valeur du bool�en en chute.
	 * 
	 * @param enChute
	 * 
	 * @see Objet#enChute
	 */
	public void setEnChute(boolean enChute) {
		this.enChute = enChute;
	}

	/**
	 * Retourne le boolean permettant de savoir si l'obet est en mouvement automatique.
	 * 
	 * @return un boolean.
	 * 
	 * @see Objet#enMouvementAuto
	 */
	public boolean isEnMouvementAuto() {
		return enMouvementAuto;
	}

	/**
	 * Met � jour la valeur du bool�en en mouvement automatique.
	 * 
	 * @param enMouvementAuto
	 * 
	 * @see Objet#enMouvementAuto
	 */
	public void setEnMouvementAuto(boolean enMouvementAuto) {
		this.enMouvementAuto = enMouvementAuto;
	}

	/**
	 * Fonction appliquant la gravit� sur un Objet
	 * 
	 * @param plateau
	 * 			L'instance du plateau sur laquelle l'objet se trouve.
	 * @param x
	 * 			La position x � appliquer � une nouvelle instance de Deplacement
	 * @param y
	 * 			La position y � appliquer � une nouvelle instance de Deplacement
	 * 
	 * @throws Exception
	 * 
	 * @see Plateau
	 */
	public abstract void gravite(Plateau plateau, int x, int y) throws Exception;
	
	/**
	 * Retourne un Objet quand l'Objet se d�place
	 * 
	 * @param plateau
	 * 			L'instance du plateau sur laquelle l'objet se d�place.
	 * @param x
	 * 			La position x � appliquer � une nouvelle instance de Deplacement
	 * @param y
	 * 			La position y � appliquer � une nouvelle instance de Deplacement
	 * 
	 * @return Une instance de l'Objet apr�s d�placement
	 * 
	 * @throws Exception
	 * 
	 * @see Plateau
	 */
	public abstract Objet seDeplace(Plateau plateau, int x, int y) throws Exception;
	
	/**
	 * Retourne un entier al�atoire
	 * 
	 * @param min
	 * 			L'entier minimum � retourner
	 * @param max
	 * 			L'entier maximum � retourner
	 * 
	 * @return Un entier compris entre min et max de fa�on al�atoire
	 */
	public int randInt(int min, int max) {
		Random random = new Random();
		return min + random.nextInt(max - min);
	}

	/**
	 * Retourne Un String
	 * 
	 * @return Un String permettant de d�crire le type de l'objet et ses diff�rents �tats
	 * 
	 * @see Objet#enChute
	 * @see Objet#enMouvementAuto
	 */
	@Override
	public String toString() {
		return "Objet [enChute=" + enChute + ", enMouvementAuto=" + enMouvementAuto + "]";
	}
	
}
