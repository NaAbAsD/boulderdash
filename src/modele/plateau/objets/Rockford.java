package modele.plateau.objets;

import modele.plateau.Plateau;

/**
 * <b>Rockford est l'Objet repr�sentant le joueur.</b>
 * <p>
 * Cette classe est caract�ris�e par l'information suivante :
 * <ul>
 * <li>L'entier repr�sentant la position en X du joueur.</li>
 * <li>L'entier repr�sentant la position en Y du joueur.</li>
 * <li>L'entier repr�sentant le nombre de vies du joueur.</li>
 * <li>L'entier repr�sentant le nombre de diamants du joueur.</li>
 * </ul>
 * </p>
 */
public class Rockford extends Objet {

	/**
	 * L'entier qui repr�sente la position x du Rockford
	 * 
	 * @see Rockford#getX()
	 * @see Rockford#setX(int)
	 */
	private int x;
	
	/**
	 * L'entier qui repr�sente la position y du Rockford
	 * 
	 * @see Rockford#getY()
	 * @see Rockford#setY(int)
	 */
	private int y;
	
	/**
	 * L'entier qui repr�sente le nombre de vies du Rockford
	 * 
	 * @see Rockford#getVie()
	 * @see Rockford#setVie(int)
	 */
	private int vie;
	
	/**
	 * L'entier qui repr�sente le nombre de diamants du Rockford
	 * 
	 * @see Rockford#getNbDiamants()
	 * @see Rockford#setNbDiamants(int)
	 */
	private int nbDiamants;
	
	/**
	 * Constructeur de Rockford
	 * 
	 * @param x
	 * 			L'entier �tant la position en x de Rockford
	 * @param y
	 * 			L'entier �tant la position en y de Rockford
	 * 
	 * @see Rockford#setX
	 * @see Rockford#setY
	 * @see Rockford#setVie(int)
	 * @see Rockford#setNbDiamants(int)
	 */
	public Rockford(int x, int y) {
		super();
		setX(x);
		setY(y);
		setVie(3);
		setNbDiamants(0);
	}
	
	/**
	 * Constructeur de Rockford
	 * 
	 * @see Rockford#setVie(int)
	 * @see Rockford#setNbDiamants(int)
	 */
	public Rockford() {
		super();
		setVie(3);
		setNbDiamants(0);
	}
	
	/**
	 * Retourne la position x de Rockford
	 * 
	 * @return un entier r�pr�sentant la position en x du joueur
	 * 
	 * @see Rockford#x
	 */
	public int getX() {
		return x;
	}

	/**
	 * Met � jour la position en x de RockFord
	 * 
	 * @param x
	 * 			L'entier repr�sentant la position en x du joueur
	 * 
	 * @see Rockford#x
	 */
	public void setX(int x) {
		this.x = x;
	}

	/**
	 * Retourne la position y de Rockford
	 * 
	 * @return un entier r�pr�sentant la position en y du joueur
	 * 
	 * @see Rockford#y
	 */
	public int getY() {
		return y;
	}

	/**
	 * Met � jour la position en y de RockFord
	 * 
	 * @param y
	 * 			L'entier repr�sentant la position en y du joueur
	 * 
	 * @see Rockford#y
	 */
	public void setY(int y) {
		this.y = y;
	}
	
	/**
	 * Fonction permettant de modifier la position de Rockford de fa�on � ce qu'il se d�place vers la gauche
	 * 
	 * @see Rockford#y
	 * @see Rockford#setY(int)
	 */
	public void deplacementGauche() {
		setY(y - 1);
	}
	
	/**
	 * Fonction permettant de modifier la position de Rockford de fa�on � ce qu'il se d�place vers la droite
	 * 
	 * @see Rockford#y
	 * @see Rockford#setY(int)
	 */
	public void deplacementDroite() {
		setY(y + 1);
	}
	
	/**
	 * Fonction permettant de modifier la position de Rockford de fa�on � ce qu'il se d�place vers le haut
	 * 
	 * @see Rockford#x
	 * @see Rockford#setX(int)
	 */
	public void deplacementHaut() {
		setX(x - 1);
	}
	
	/**
	 * Fonction permettant de modifier la position de Rockford de fa�on � ce qu'il se d�place vers le bas
	 * 
	 * @see Rockford#x
	 * @see Rockford#setX(int)
	 */
	public void deplacementBas() {
		setX(x + 1);
	}

	/**
	 * Retourne le nombre de vies de Rockford
	 * 
	 * @return l'entier repr�sentant le nombre de vies du joueur
	 * 
	 * @see Rockford#vie
	 */
	public int getVie() {
		return vie;
	}

	/**
	 * Met � jour le nombre de vies de Rockford
	 * 
	 * @param vie
	 * 			L'entier repr�sentant le nombre de vies du joueur
	 * 
	 * @see Rockford#vie
	 */
	public void setVie(int vie) {
		this.vie = vie;
	}
	
	/**
	 * Fonction permettant d'enlever un point de vie � Rockford
	 * 
	 * @see Rockford#vie
	 */
	public void perdreVie() {
		this.vie--;
	}

	/**
	 * Retourne le nombre de diamants de Rockford
	 * 
	 * @return l'entier repr�sentant le nombre de diamants du joueur
	 * 
	 * @see Rockford#nbDiamants
	 */
	public int getNbDiamants() {
		return nbDiamants;
	}

	/**
	 * Met � jour le nombre de diamants de Rockford
	 * 
	 * @param vie
	 * 			L'entier repr�sentant le nombre de diamants du joueur
	 * 
	 * @see Rockford#nbDiamants
	 */
	public void setNbDiamants(int nbDiamants) {
		this.nbDiamants = nbDiamants;
	}
	
	
	/**
	 * Augmente de 1 le nombre de diamants de Rockford
	 *
	 * @see Rockford#nbDiamants
	 */
	public void gagnerDiamant() {
		this.nbDiamants++;
	}

	/**
	 * Gravit� non appliqu�e sur Rockford
	 * 
	 * @see Objet#gravite(Plateau, int, int)
	 */
	@Override
	public void gravite(Plateau plateau, int x, int y) {}
	
	/**
	 * Retourne null
	 * 
	 * @return null car le Rocher ne se d�place pas automatiquement
	 * 
	 * @see Objet
	 * @see Objet#seDeplace(Plateau, int, int)
	 */
	@Override
	public Objet seDeplace(Plateau plateau, int x, int y) {
		return null;
	}

	/**
	 * Retourne les informations sur le joueur
	 * 
	 * @return le String comportant les informations du joueur
	 * 
	 * @see Rockford#x
	 * @see Rockford#y
	 * @see Rockford#vie
	 * @see Rockford#nbDiamants
	 */
	public String infosJoueur() {
		return "Rockford [x=" + x + ", y=" + y + ", vie=" + vie + ", nbDiamants=" + nbDiamants + "]";
	}

	/**
	 * Retourne le String de Rockford
	 * 
	 * @see Objet#toString()
	 */
	@Override
	public String toString() {
		return "J";
	}

}
