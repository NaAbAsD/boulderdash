package modele.plateau;

import java.io.File;
import java.util.Scanner;

import modele.exceptions.BoulderException;
import modele.plateau.moteur.Deplacement;
import modele.plateau.objets.Acier;
import modele.plateau.objets.Diamant;
import modele.plateau.objets.Monstre;
import modele.plateau.objets.Objet;
import modele.plateau.objets.Rocher;
import modele.plateau.objets.Rockford;
import modele.plateau.objets.Sortie;
import modele.plateau.objets.Terre;
import modele.plateau.objets.Vide;

/**
 * <b>Grille est la classe repr�sentant la grille du plateau du Boulderdash.</b>
 * <p>
 * Cette classe est caract�ris�e par les informations suivantes :
 * <ul>
 * <li>Un tableau d'objet permettant de cr�er une grille.</li>
 * <li>Une instance de Rockford d�fini sur la Grille.</li>
 * <li>Un entier repr�sentant la hauteur de la grille.</li>
 * <li>Un entier repr�sentant la largeur de la grille.</li>
 * </ul>
 * </p>
 * 
 * @see Rockford
 * @see Objet
 */
public class Grille {
	
	/**
	 * <p>Un tableau d'objet permettant de cr�er une grille.</p>
	 * 
	 * @see Objet
	 * 
	 * @see Grille#getGrille()
	 * @see Grille#setGrille(Objet[][])
	 */
	private Objet[][] grille;
	
	/**
	 * <p>L'instance de Rockford correspondant au joueur sur la grille</p>
	 * 
	 * @see Rockford
	 * 
	 * @see Grille#getJoueur()
	 * @see Grille#setJoueur(Rockford)
	 */
	private Rockford joueur;

	/**
	 * L'entier repr�sentant la hauteur de la grille.
	 * 
	 * @see Grille#getHauteur()
	 * @see Grille#setHauteur(int)
	 */
	private int hauteur;
	
	/**
	 * L'entier repr�sentant la largeur de la grille.
	 * 
	 * @see Grille#getLargeur()
	 * @see Grille#setLargeur(int)
	 */
	private int largeur;
	
	/**
	 * La hauteur et largeur "par d�faut"
	 */
	public static final int HAUTEUR = 10;
	public static final int LARGEUR = 10;
	
	/**
	 * Constructeur de Grille avec des hauteurs et largeurs donn�es
	 * 
	 * @param hauteur
	 * @param largeur
	 * 
	 * @see Grille#setGrille(int, int)
	 * @see Grille#setHauteur(int)
	 * @see Grille#setLargeur(int)
	 */
	public Grille(int hauteur, int largeur) {
		setGrille(hauteur, largeur);
		setHauteur(hauteur);
		setLargeur(largeur);
	}
	
	/**
	 * Constructeur de Grille par d�faut
	 * 
	 * @param hauteur
	 * @param largeur
	 * 
	 * @see Grille#setGrille(int, int)
	 * @see Grille#setHauteur(int)
	 * @see Grille#setLargeur(int)
	 * 
	 * @see Grille#HAUTEUR
	 * @see Grille#LARGEUR
	 */
	public Grille() {
		setGrille(HAUTEUR, LARGEUR);
		setHauteur(HAUTEUR);
		setLargeur(LARGEUR);
	}
	
	/**
	 * Retourne le tableau d'Objet correspondant � la grille.
	 * 
	 * @return un tableau d'Objet
	 * 
	 * @see Objet
	 * 
	 * @see Grille#grille
	 */
	public Objet[][] getGrille() {
		return grille;
	}
	
	/**
	 * Met � jour le tableau d'Objet correspondant � la grille.
	 * 
	 * @param grille
	 * 
	 * @see Objet
	 * 
	 * @see Grille#grille
	 */
	public void setGrille(Objet[][] grille) {
		this.grille = grille;
	}
	
	/**
	 * Met � jour le tableau d'Objet correspondant � la grille avec une hauteur et une largeur donn�es.
	 * 
	 * @param hauteur
	 * @param largeur
	 */
	public void setGrille(int hauteur, int largeur) {
		this.grille = new Objet[hauteur][largeur];
		setHauteur(hauteur);
		setLargeur(largeur);
	}

	/**
	 * Retourne un entier correspondant � la hauteur de l'instance de Grille.
	 * 
	 * @return un entier
	 * 
	 * @see Grille#hauteur
	 */
	public int getHauteur() {
		return hauteur;
	}

	/**
	 * Met � jour la hauteur de l'instance de Grille
	 * 
	 * @param hauteur
	 * 
	 * @see Grille#hauteur
	 */
	public void setHauteur(int hauteur) {
		this.hauteur = hauteur;
	}

	/**
	 * Retourne un entier correspondant � la largeur de l'instance de Grille.
	 * 
	 * @return un entier
	 * 
	 * @see Grille#largeur
	 */
	public int getLargeur() {
		return largeur;
	}
	
	/**
	 * Met � jour la largeur de l'instance de Grille
	 * 
	 * @param largeur
	 * 
	 * @see Grille#largeur
	 */
	public void setLargeur(int largeur) {
		this.largeur = largeur;
	}
	
	/**
	 * Retourne l'instance de Rockford utilis�e par Grille.
	 * 
	 * @return une instance de Rockford
	 * 
	 * @see Grille#joueur
	 */
	public Rockford getJoueur() {
		return joueur;
	}

	/**
	 * Met � jour l'instance de Rockford utilis�e par Grille.
	 * 
	 * @param joueur
	 * 
	 * @see Grille#joueur
	 */
	public void setJoueur(Rockford joueur) {
		this.joueur = joueur;
	}

	/**
	 * Retourne l'Objet en position (l,c) de l'instance de Grille.
	 * 
	 * @param l
	 * @param c
	 * 
	 * @return une instance d'Objet.
	 * 
	 * @see Objet
	 * 
	 * @see Grille#grille
	 */
	public Objet getObjet(int l, int c) {
		return grille[l][c];
	}
	
	/**
	 * Met � jour l'Objet en position l,c avec une autre instance d'Objet.
	 * 
	 * @param l
	 * @param c
	 * @param objet
	 * 
	 * @see Objet
	 * 
	 * @see Grille#grille
	 */
	public void setObjet(int l, int c, Objet objet) {
		grille[l][c] = objet;
	}
	
	/**
	 * Fonction affichant la grille dans la console.
	 * 
	 * @see Grille#getHauteur()
	 * @see Grille#getLargeur()
	 * @see Grille#grille
	 */
	public void printGrille() {
		for (int i = 0; i < getHauteur(); i++) {
			for (int j = 0; j < getLargeur(); j++) {
					System.out.print(grille[i][j] + "(" + i + ", " + j + ") ");
			}
		System.out.println();
		}
		System.out.println();
	}
	
	/**
	 * Fonction echangeant la position d'un Objet avec un Objet Vide gr�ce � un deplacement
	 * 
	 * @param d
	 * 
	 * @see Deplacement
	 * @see Deplacement#getObjet()
	 * @see Deplacement#getlDep()
	 * @see Deplacement#getcDep()
	 * @see Deplacement#getlArr()
	 * @see Deplacement#getcArr()
	 * 
	 */
	public void swap(Deplacement d) {
		Objet swapper = d.getObjet();
		setObjet(d.getlDep(), d.getcDep(), new Vide());
		setObjet(d.getlArr(), d.getcArr(), swapper);
	}
	
	/**
	 * Fonction echangeant la position d'un Objet avec un Objet Vide.
	 * 
	 * @param l1
	 * @param c1
	 * @param l2
	 * @param c2
	 * 
	 * @see Grille#getObjet(int, int)
	 */
	public void swap(int l1, int c1, int l2, int c2) {
		Objet swapper = getObjet(l1, c1);
		setObjet(l1, c1, new Vide());
		setObjet(l2, c2, swapper);
	}
	
	/**
	 * <p>Fonction permettant de lire un fichier pass� en param�tre afin de cr�er une grille et un plateau,
	 * en fonction du caract�re ecrit dans le fichier.</p>
	 * 
	 * @param file
	 * 
	 * @throws Exception
	 * 
	 * @see Rockford
	 * @see BoulderException
	 * 
	 * @see Grille#setObjet(int, int, Objet)
	 * @see Grille#setGrille(int, int)
	 * @see Grille#setJoueur(Rockford)
	 */
	public void readGrilleFromFile(File file) throws Exception {
		if (file == null) return;
		Scanner scanner = null;
		Rockford player = null;
		try {
			scanner = new Scanner(file);
			int i = 0;
			if (scanner.hasNextLine()) {
		        String premiereLigne = scanner.nextLine();
		        String taille[] = premiereLigne.split(" ");
	        	if (taille.length > 1) {
	        		int largeur = Integer.parseInt(taille[0]);
	        		int hauteur = Integer.parseInt(taille[1]);
	        		if (largeur > 0 && hauteur > 0) {
	        			setGrille(hauteur, largeur);
	        		} else throw new BoulderException(
	        				"Erreur : La taille de la grille de la carte s�lectionn�e est invalide\n"
	        				+ "Le chargement de la carte est interrompu.");
	        	}
			}
			while (scanner.hasNextLine()) {
				String ligne = scanner.nextLine();
				if (ligne.length() == getLargeur()) {
					for (int j = 0; j < ligne.length(); j++) {
						switch(ligne.charAt(j)) {
							case 'V': {
								setObjet(i, j, new Vide());
								break;
							}
							case 'R': {
								setObjet(i, j, new Rocher());
								break;
							}
							case 'M': {
								setObjet(i, j, new Monstre());
								break;
							}
							case 'A': {
								setObjet(i, j, new Acier());
								break;
							}
							case 'D': {
								setObjet(i, j, new Diamant());
								break;
							}
							case 'T': {
								setObjet(i, j, new Terre());
								break;
							}
							case 'S': {
								setObjet(i, j, new Sortie());
								break;
							}
							case 'J': {
								if (player != null) throw new BoulderException(
										"Erreur : Il y a plusieurs Rockford sur la grille.\nLe chargement de la carte est interrompu.");
								player = new Rockford(i, j);
								setObjet(i, j, player);
								setJoueur(player);
								break;
							}
						}
						
					}
				} else throw new BoulderException(
						"Erreur : La ligne %d du la carte est invalide\nLe chargement de la carte est interrompu.");
			i++;
			}
		} catch(Exception e) {
			throw e;
		} finally {
		    if (scanner != null) {
		    	scanner.close();
		    }
		}
	}

	/**
	 * Retourne un String exprimant toute les informations necessaire � la grille.
	 * 
	 * @see Grille#grille
	 * @see Grille#hauteur
	 * @see Grille#largeur
	 * @see Grille#joueur
	 */
	@Override
	public String toString() {
		String affGrille = new String();
		for (int i = 0; i < getHauteur(); i++) {
			for (int j = 0; j < getLargeur(); j++) {
					affGrille += grille[i][j] + " ";
			}
			affGrille += "\n";
		}
		return "Grille [grille=\n" + affGrille + "joueur=" + joueur + ", hauteur=" + hauteur
				+ ", largeur=" + largeur + "]";
	}
	
}