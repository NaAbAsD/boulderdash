package modele.plateau;

import modele.cor.interactions.Interaction;
import modele.exceptions.BoulderException;

import java.io.File;
import java.util.ArrayList;

import modele.cor.detecteurs.DetecteurInteractions;
import modele.plateau.moteur.Deplacement;
import modele.plateau.objets.Objet;
import modele.plateau.objets.Rockford;

/**
 * <b>Plateau est la classe repr�sentant le plateau du jeu.</b>
 * <p>
 * Un plateau est caract�ris� par les informations suivantes :
 * <ul>
 * <li>La grille contenant les diff�rents objets.</li>
 * <li>Le premier d�tecteur capable d'identifier les probl�mes.</li>
 * <li>Le niveau actuel du jeu.</li>
 * <li>Le type du niveau actuel.</li>
 * </ul>
 * </p>
 * 
 * @see Grille
 * @see DetecteurInteractions
 */
public class Plateau {
	
	/**
	 * La grille d'objets du jeu.
	 * 
	 * @see Plateau#getGrille()
	 * @see Plateau#setGrille(Grille)
	 */
	private Grille grille;
	
	/**
	 * Le premier d�tecteur d'identification.
	 * 
	 * @see Plateau#getPremierDetecteur()
	 * @see Plateau#setPremierDetecteur(DetecteurInteractions)
	 */
	private DetecteurInteractions premierDetecteur;
	
	/**
	 * Le niveau actuel.
	 * 
	 * @see Plateau#getNiveau()
	 * @see Plateau#setNiveau(int)
	 */
	private int niveau;
	
	/**
	 * Le type du niveau actuel.
	 * 
	 * @see Plateau#isNiveauCustom()
	 * @see Plateau#setNiveauCustom(boolean)
	 */
	private boolean niveauCustom;

	/**
	 * Constructeur par d�faut du plateau.
	 * La grille est construite par d�faut.
	 * 
	 * @see Grille
	 * @see Plateau#setGrille(Grille)
	 */
	public Plateau() {
		setGrille(new Grille());
	}
	
	/**
	 * Constructeur du plateau � l'aide de sa taille.
	 * La grille est construite � l'aide de cette taille.
	 * 
	 * @param hauteur
	 * 			La hauteur de la grille.
	 * @param largeur
	 * 			La largeur de la grille.
	 * 
	 * @see Grille
	 * @see Plateau#setGrille(Grille)
	 */
	public Plateau(int hauteur, int largeur) {
		setGrille(new Grille(hauteur, largeur));
	}
	
	/**
	 * Constructeur du plateau � l'aide de son premier detecteur.
	 * La grille est construite par d�faut.
	 * 
	 * @param premierDetecteur
	 * 			Le premier d�tecteur de probl�mes.
	 * 
	 * @see Grille
	 * @see Plateau#setGrille(Grille)
	 * 
	 * @see DetecteurInteractions
	 * @see Plateau#setPremierDetecteur(DetecteurInteractions)
	 */
	public Plateau(DetecteurInteractions premierDetecteur) {
		setGrille(new Grille());
		setPremierDetecteur(premierDetecteur);
	}
	
	/**
	 * Constructeur du plateau � l'aide de sa taille et de son premier detecteur.
	 * La grille est construite � l'aide de cette taille.
	 * 
	 * @param hauteur
	 * 			La hauteur de la grille.
	 * @param largeur
	 * 			La largeur de la grille.
	 * @param premierDetecteur
	 * 			Le premier d�tecteur de probl�mes.
	 * 
	 * @see Grille
	 * @see Plateau#setGrille(Grille)
	 * 
	 * @see DetecteurInteractions
	 * @see Plateau#setPremierDetecteur(DetecteurInteractions)
	 */
	public Plateau(int hauteur, int largeur, DetecteurInteractions premierDetecteur) {
		setGrille(new Grille(hauteur, largeur));
		setPremierDetecteur(premierDetecteur);
	}
	
	/**
	 * Retourne la grille.
	 * 
	 * @see Grille
	 * 
	 * @see Plateau#grille
	 */
	public Grille getGrille() {
		return grille;
	}

	/**
	 * Met � jour la grille.
	 * 
	 * @see Grille
	 * 
	 * @see Plateau#grille
	 */
	public void setGrille(Grille grille) {
		this.grille = grille;
	}
	
	/**
	 * Retourne le premier d�tecteur.
	 * 
	 * @see DetecteurInteractions
	 * 
	 * @see Plateau#premierDetecteur
	 */
	public DetecteurInteractions getPremierDetecteur() {
		return premierDetecteur;
	}

	/**
	 * Met � jour le premier d�tecteur.
	 * 
	 * @see DetecteurInteractions
	 * 
	 * @see Plateau#premierDetecteur
	 */
	public void setPremierDetecteur(DetecteurInteractions premierDetecteur) {
		this.premierDetecteur = premierDetecteur;
	}
	
	/**
	 * Retourne le niveau actuel.
	 * 
	 * @see Plateau#niveau
	 */
	public int getNiveau() {
		return niveau;
	}

	/**
	 * Met � jour le niveau actuel.
	 * 
	 * @see Plateau#niveau
	 */
	public void setNiveau(int niveau) {
		this.niveau = niveau;
	}

	/**
	 * Retourne le type de niveau.
	 * 
	 * @see Plateau#niveauCustom
	 */
	public boolean isNiveauCustom() {
		return niveauCustom;
	}

	/**
	 * Met � jour le type de niveau.
	 * 
	 * @see Plateau#niveauCustom
	 */
	public void setNiveauCustom(boolean niveauCustom) {
		this.niveauCustom = niveauCustom;
	}

	/**
	 * Retourne le joueur.
	 * 
	 * @see Grille#getJoueur()
	 */
	public Rockford getJoueur() {
		return grille.getJoueur();
	}
	
	/**
	 * Met � jour le joueur.
	 * 
	 * @see Grille#setJoueur(Rockford)
	 */
	public void setJoueur(Rockford joueur) {
		grille.setJoueur(joueur);
	}
	
	/**
	 * Retourne la hauteur de la grille.
	 * 
	 * @see Grille#getHauteur()
	 */
	public int getHauteur() {
		return grille.getHauteur();
	}

	/**
	 * Retourne la largeur de la grille.
	 * 
	 * @see Grille#getLargeur()
	 */
	public int getLargeur() {
		return grille.getLargeur();
	}

	/**
	 * Retourne l'objet aux coordonn�es (l, c) de la grille.
	 * 
	 * @see Grille#getObjet(int, int)
	 */
	public Objet getObjet(int l, int c) {
		return grille.getObjet(l, c);
	}

	/**
	 * Met � jour l'objet aux coordonn�es (l, c) de la grille.
	 * 
	 * @see Grille#getObjet(int, int)
	 */
	public void setObjet(int l, int c, Objet objet) {
		grille.setObjet(l, c, objet);
	}

	/**
	 * Echange les deux objets pr�sents aux 2 couples de coordonn�es li�es au d�placement
	 * 
	 * @see Grille#swap(Deplacement)
	 * @see Deplacement
	 */
	public void swap(Deplacement d) {
		grille.swap(d);
	}

	/**
	 * Echange les deux objets pr�sents aux 2 couples de coordonn�es donn�s en param�tres.
	 * 
	 * @see Grille#swap(Deplacement)
	 */
	public void swap(int x1, int y1, int x2, int y2) {
		grille.swap(x1, y1, x2, y2);
	}
	
	/**
	 * Affiche la grille.
	 * 
	 * @see Grille#printGrille()
	 */
	public void printGrille() {
		grille.printGrille();
	}

	/**
	 * <b>Fonction retournant un bool�en</b>
	 * <p>Elle retourne vrai lorsque l'interaction a �t� d�tect�e par le detecteur correspondant 
	 * et traiter par l'interaction correspondante, sinon si le d�tecteur n'a d�tect� cette interaction, 
	 * la fonction retourne faux.</p>
	 * 
	 * @param dep
	 * 
	 * @return un bool�en
	 * 
	 * @throws Exception
	 * 
	 * @see Deplacement
	 * @see Deplacement#isValide()
	 * 
	 * @see DetecteurInteractions
	 * @see DetecteurInteractions#detecter(Deplacement)
	 * 
	 * @see Interaction
	 * @see Interaction#traiter()
	 * 
	 * @see Plateau#premierDetecteur
	 */
	public boolean jouer(Deplacement dep) throws Exception {
		assert(premierDetecteur != null) : "Erreur : la chaine de responsabilit� est vide";
		if (!dep.isValide()) return false; // On ne peut pas se deplacer
		Interaction interaction = premierDetecteur.detecter(dep);
		if (interaction == null) {
			return false;
		} else {
			try {
				interaction.traiter();
				return true;
			} catch (Exception e) {
				throw e;
			}
		}
	}
	
	/**
	 * <p>Fonction permettant d'ajouter un detecteur d'interactions � la liste de Detecteur 
	 * et d'ainsi modifier le premierDetecteur par celui-ci.</p>
	 * 
	 * @param d
	 * 
	 * @see DetecteurInteractions
	 * @see DetecteurInteractions#setSuivant(DetecteurInteractions)
	 * 
	 * @see Plateau#premierDetecteur
	 */
	public void ajouterDetecteur(DetecteurInteractions d) {
		assert(d != null) : "Le dectecteur ne doit pas �tre null";
		d.setSuivant(premierDetecteur);
		premierDetecteur = d;
	}
	
	/**
	 * <b>Fonction permettant d'appliquer la gravit� sur le plateau.</b>
	 * <p>Elle appelle la gravit� pour chaque objet du plateau</p>
	 * 
	 * @throws Exception
	 * 
	 * @see Objet
	 * @see Objet#gravite(Plateau, int, int)
	 * 
	 * @see Plateau#getHauteur()
	 * @see Plateau#getLargeur()
	 * @see Plateau#getObjet(int, int)
	 */
	public void appliquerGravite() throws Exception {
		for (int i = getHauteur() - 2; i >= 0; i--) { // Pas besoin de check la derni�re ligne
			for (int j = 0; j < getLargeur(); j++) {
				try {
					getObjet(i, j).gravite(this, i, j);
				} catch (Exception e) {
					throw e;
				}
			}
		}
	}

	/**
	 * <b>Fonction permettant d'appliquer le Deplacement Automatique sur le Plateau</b>
	 * <p>Elle appelle le Deplacement de chaque objet du Plateau.
	 * Pour elle ajoute l'objet d�plac� � la liste d'Objet.
	 * Finalement pour chaque objet de la liste, la fonction set le mouvement � false.</p>
	 * 
	 * @throws Exception
	 * 
	 * @see Objet
	 * @see Objet#isEnMouvementAuto()
	 * @see Objet#seDeplace(Plateau, int, int)
	 * @see Objet#setEnMouvementAuto(boolean)
	 * 
	 * @see Plateau#getHauteur()
	 * @see Plateau#getLargeur()
	 * @see Plateau#getObjet(int, int)
	 */
	public void appliquerDeplacementAutomatique() throws Exception {
		ArrayList<Objet> moved = new ArrayList<Objet>(); // AL des objets d�j� d�plac�s
		for (int i = getHauteur() - 1; i >= 0; i--) {
			for (int j = 0; j < getLargeur(); j++) {
				if (!getObjet(i, j).isEnMouvementAuto()) { // On ne l'a pas d�j� d�plac�
					try {
						Objet obj = getObjet(i, j).seDeplace(this, i, j);
						if (obj != null) {
							moved.add(obj);
						}
					} catch (Exception e) {
						throw e;
					}
				}
			}
		}
		for (Objet o : moved) { // On retire le mouvement
			o.setEnMouvementAuto(false);
		}
	}
	
	/**
	 * <p>niveauSuivant est appel�e lorsqu'un changement de niveau est requis.
	 * Elle incr�mente la valeur du niveau, puis cherche si ce niveau existe.
	 * Dans le cas contraire une exception est envoy�e.</p>
	 * 
	 * @throws Expcetion
	 * 
	 * @see Grille
	 * @see Grille#readGrilleFromFile(File)
	 * @see Grille#getJoueur()
	 * 
	 * @see Rockford
	 * 
	 * @see BoulderException
	 */
	public void niveauSuivant() throws Exception {
		if (isNiveauCustom()) throw new BoulderException();
		this.niveau++;
		int nbDiamants = getJoueur().getNbDiamants();
		int nbVies = getJoueur().getVie();
		File suivant = new File("ressources/map/niveaux/niveau-" + Integer.toString(niveau) + ".boulderdash");
		if (suivant.exists()) {
			grille.readGrilleFromFile(suivant);
			getJoueur().setVie(nbVies);
			getJoueur().setNbDiamants(nbDiamants);
		} else throw new BoulderException();
	}
	
}