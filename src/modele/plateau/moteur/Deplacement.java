package modele.plateau.moteur;

import modele.plateau.Plateau;
import modele.plateau.objets.Objet;

/**
 * <b>Deplacement est la classe repr�sentant un d�placement d'Objet dans le cadre du d�roulement du jeu.
 * Ces d�placements peuvent �tre de multiples directions et sont utilis�s notamment pour le d�placement du joueur mais aussi
 * pour la physique du jeu.</b>
 * <p>
 * Un d�placement est caract�ris� par les informations suivantes :
 * <ul>
 * <li>Le plateau contenant le jeu actuel.</li>
 * <li>L'objet cibl� par ce d�placement.</li>
 * <li>La ligne de d�part du d�placement.</li>
 * <li>La colonne de d�part du d�placement.</li>
 * <li>La ligne d'arriv�e du d�placement.</li>
 * <li>La colonne d'arriv�e du d�placement.</li>
 * <li>La direction du d�placement.</li>
 * </ul>
 * </p>
 * 
 * @see Plateau
 * @see Objet
 */
public class Deplacement {
	
	/**
	 * Le plateau actuel du jeu sur lequel le d�placement � lieu.
	 * 
	 * @see Deplacement#getPlateau()
	 * @see Deplacement#setPlateau(Plateau)
	 */
	private Plateau plateau;
	
	/**
	 * L'objet subissant le d�placement.
	 * 
	 * @see Deplacement#getObjet()
	 * @see Deplacement#setObjet(Objet)
	 */
	private Objet objet;
	
	/**
	 * La ligne de d�part.
	 * 
	 * @see Deplacement#getlDep()
	 * @see Deplacement#setlDep(int)
	 */
	private int lDep;
	
	/**
	 * La colonne de d�part.
	 * 
	 * @see Deplacement#getcDep()
	 * @see Deplacement#setcDep(int)
	 */
	private int cDep;
	
	/**
	 * La ligne d'arriv�e.
	 * 
	 * @see Deplacement#getlArr()
	 * @see Deplacement#setlArr(int)
	 */
	private int lArr;
	
	/**
	 * La colonne d'arriv�e.
	 * 
	 * @see Deplacement#getcArr()
	 * @see Deplacement#setcArr(int)
	 */
	private int cArr;
	
	/**
	 * La direction dans laquel se d�place l'objet.
	 * 
	 * @see Deplacement#HAUT
	 * @see Deplacement#DROITE
	 * @see Deplacement#BAS
	 * @see Deplacement#GAUCHE
	 */
	private int dir;
	
	/**
	 * Un d�placement vers le haut.
	 */
	public static final int HAUT = 1;
	
	/**
	 * Un d�placement vers la droite.
	 */
	public static final int DROITE = 2;
	
	/**
	 * Un d�placement vers le bas.
	 */
	public static final int BAS = 3;
	
	/**
	 * Un d�placement vers la gauche.
	 */
	public static final int GAUCHE = 4;
	
	/**
	 * Constructeur du d�placement "classique"
	 * 
	 * @param plateau
	 * 			Le plateau du jeu courant.
	 * @param objet
	 * 			L'objet sur lequel le d�placement � lieu.
	 * @param lDep
	 * 			La ligne de d�part du d�placement.
	 * @param cDep
	 * 			La colonne de d�part du d�placement.
	 * @param dir
	 * 			La direction du d�placement
	 * 
	 * @see Plateau
	 * @see Deplacement#setPlateau(Plateau)
	 * 
	 * @see Objet
	 * @see Deplacement#setObjet(Objet)
	 * 
	 * @see Deplacement#setlDep(int)
	 * @see Deplacement#setcDep(int)
	 * @see Deplacement#setDirection(int)
	 */
	public Deplacement(Plateau plateau, Objet objet, int lDep, int cDep, int dir) {
		setPlateau(plateau);
		setObjet(objet);
		setlDep(lDep);
		setcDep(cDep);
		setDirection(dir);
	}
	
	/**
	 * Constructeur du d�placement � l'aide d'un autre d�placement.
	 * Cela est tr�s utile lors d'une application r�cursive d'un d�placement tel que la gravit�.
	 * 
	 * @param dep
	 * 			Le d�placement pr�c�dent.
	 * @param objet
	 * 			L'objet sur lequel le d�placement � lieu.
	 * 
	 * @see Plateau
	 * @see Deplacement#setPlateau(Plateau)
	 * 
	 * @see Objet
	 * @see Deplacement#setObjet(Objet)
	 * 
	 * @see Deplacement#setlDep(int)
	 * @see Deplacement#setcDep(int)
	 * @see Deplacement#setDirection(int)
	 */
	public Deplacement(Deplacement dep, Objet objet) {
		setPlateau(dep.getPlateau());
		setObjet(objet);
		setlDep(dep.getlArr());
		setcDep(dep.getcArr());
		setDirection(dep.getDir());
	}

	/**
	 * Retourne le plateau du jeu courant.
	 * 
	 * @see Plateau
	 * 
	 * @see Deplacement#plateau
	 */
	public Plateau getPlateau() {
		return plateau;
	}

	/**
	 * Met � jour le plateau du jeu courant.
	 * 
	 * @see Plateau
	 * 
	 * @see Deplacement#plateau
	 */
	public void setPlateau(Plateau plateau) {
		this.plateau = plateau;
	}

	/**
	 * Retourne l'objet subissant le d�placement.
	 * 
	 * @see Objet
	 * 
	 * @see Deplacement#objet
	 */
	public Objet getObjet() {
		return objet;
	}

	/**
	 * Met � jour l'objet subissant le d�placement.
	 * 
	 * @see Objet
	 * 
	 * @see Deplacement#objet
	 */
	public void setObjet(Objet objet) {
		this.objet = objet;
	}

	/**
	 * Retourne la ligne de d�part du d�placement.
	 * 
	 * @see Deplacement#lDep
	 */
	public int getlDep() {
		return lDep;
	}

	/**
	 * Met � jour la ligne de d�part du d�placement.
	 * 
	 * @see Deplacement#lDep
	 */
	public void setlDep(int lDep) {
		this.lDep = lDep;
	}

	/**
	 * Retourne la colonne de d�part du d�placement.
	 * 
	 * @see Deplacement#cDep
	 */
	public int getcDep() {
		return cDep;
	}

	/**
	 * Met � jour la colonne de d�part du d�placement.
	 * 
	 * @see Deplacement#cDep
	 */
	public void setcDep(int cDep) {
		this.cDep = cDep;
	}

	/**
	 * Retourne la ligne d'arriv�e du d�placement.
	 * 
	 * @see Deplacement#lArr
	 */
	public int getlArr() {
		return lArr;
	}

	/**
	 * Met � jour la ligne d'arriv�e du d�placement.
	 * 
	 * @see Deplacement#lArr
	 */
	public void setlArr(int lArr) {
		this.lArr = lArr;
	}

	/**
	 * Retourne la colonne d'arriv�e du d�placement.
	 * 
	 * @see Deplacement#cArr
	 */
	public int getcArr() {
		return cArr;
	}

	/**
	 * Met � jour la colonne d'arriv�e du d�placement.
	 * 
	 * @see Deplacement#cArr
	 */
	public void setcArr(int cArr) {
		this.cArr = cArr;
	}
	
	/**
	 * Retourne la direction du d�placement.
	 * 
	 * @see Deplacement#dir
	 * @see Deplacement#HAUT
	 * @see Deplacement#DROITE
	 * @see Deplacement#BAS
	 * @see Deplacement#GAUCHE
	 */
	public int getDir() {
		return dir;
	}

	/**
	 * Met � jour la direction du d�placement.
	 * 
	 * @see Deplacement#dir
	 * @see Deplacement#HAUT
	 * @see Deplacement#DROITE
	 * @see Deplacement#BAS
	 * @see Deplacement#GAUCHE
	 */
	public void setDir(int dir) {
		this.dir = dir;
	}
	
	/**
	 * <p>setDirection met � jour la direction du d�placement et r�alise le "calcul" des coordonn�es d'arriv�e du d�placement.
	 * Si la valeur de direction est invalide, un d�placement bas est effectu�.</p>
	 * 
	 * @see Deplacement#setDir(int)
	 * @see Deplacement#HAUT
	 * @see Deplacement#DROITE
	 * @see Deplacement#BAS
	 * @see Deplacement#GAUCHE
	 * 
	 * @see Deplacement#deplacementHaut()
	 * @see Deplacement#deplacementDroite()
	 * @see Deplacement#deplacementBas()
	 * @see Deplacement#deplacementGauche()
	 */
	public void setDirection(int dir) {
		setDir(dir);
		switch (dir) {
			case (HAUT): {
				deplacementHaut();
				break;
			}
			case (DROITE): {
				deplacementDroite();
				break;
			}
			case (BAS): {
				deplacementBas();
				break;
			}
			case (GAUCHE): {
				deplacementGauche();
				break;
			}
			default: {
				deplacementBas();
			}
		}
	}

	/**
	 * <p>deplacementGauche r�alise le calcul et met � jour les valeurs d'arriv�e pour le d�placement vers la gauche.</p>
	 * 
	 * @see Deplacement#setlArr(int)
	 * @see Deplacement#setcArr(int)
	 * @see Deplacement#getlDep()
	 * @see Deplacement#getcDep()
	 */
	public void deplacementGauche() {
		setlArr(getlDep());
		setcArr(getcDep() - 1);
	}
	
	/**
	 * <p>deplacementDroite r�alise le calcul et met � jour les valeurs d'arriv�e pour le d�placement vers la droite.</p>
	 * 
	 * @see Deplacement#setlArr(int)
	 * @see Deplacement#setcArr(int)
	 * @see Deplacement#getlDep()
	 * @see Deplacement#getcDep()
	 */
	public void deplacementDroite() {
		setlArr(getlDep());
		setcArr(getcDep() + 1);
	}
	
	/**
	 * <p>deplacementHaut r�alise le calcul et met � jour les valeurs d'arriv�e pour le d�placement vers le haut.</p>
	 * 
	 * @see Deplacement#setlArr(int)
	 * @see Deplacement#setcArr(int)
	 * @see Deplacement#getlDep()
	 * @see Deplacement#getcDep()
	 */
	public void deplacementHaut() {
		setlArr(getlDep() - 1);
		setcArr(getcDep());
	}
	
	/**
	 * <p>deplacementBas r�alise le calcul et met � jour les valeurs d'arriv�e pour le d�placement vers le bas.</p>
	 * 
	 * @see Deplacement#setlArr(int)
	 * @see Deplacement#setcArr(int)
	 * @see Deplacement#getlDep()
	 * @see Deplacement#getcDep()
	 */
	public void deplacementBas() {
		setlArr(getlDep() + 1);
		setcArr(getcDep());
	}
	
	/**
	 * <p>isValide v�rifie alors si le d�placement obtenu par calcul est correct, c'est � dire dans les limites du plateau.</p>
	 * 
	 * @see Plateau
	 * @see Plateau#getLargeur()
	 * @see Plateau#getHauteur()
	 */
	public boolean isValide() {
		return (this.lArr >= 0 && this.lArr <= plateau.getLargeur() - 1) 
				&& (this.cArr >= 0 && this.cArr <= plateau.getHauteur() - 1);
	}

	/**
	 * <p>Retourne la description d'un d�placement.</p>
	 */
	@Override
	public String toString() {
		return "Deplacement [objet=" + objet + ", xDep=" + lDep + ", yDep=" + cDep + ", xArr="
				+ lArr + ", yArr=" + cArr + ", dir=" + dir + "]";
	}
	
}
