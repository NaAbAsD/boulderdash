package modele.plateau.moteur;

import application.ControleurFenetreAccueil;
import javafx.scene.control.Alert.AlertType;
import modele.obs.Observable;
import modele.obs.Observateur;
import modele.plateau.Plateau;
import ui.PanneauFooter;

/**
 * <b>MoteurGravite est la classe g�rant la gravit� de certains objets comme le Rocher.
 * Cette classe rentre dans le cadre du design pattern Observable/Observateur.</b>
 * <p>
 * Un MoteurGravite est caract�ris� par les informations suivantes :
 * <ul>
 * <li>La fen�tre du jeu � mettre � jour.</li>
 * <li>Le plateau du jeu courant.</li>
 * </ul>
 * </p>
 * 
 * @see ControleurFenetreAccueil
 * @see Plateau
 */
public class MoteurGravite implements Observateur {

	/**
	 * La fen�tre du jeu � mettre � jour.
	 * 
	 * @see MoteurDeplacementAuto#getUi()
	 * @see MoteurDeplacementAuto#setUi(ControleurFenetreAccueil)
	 */
	private ControleurFenetreAccueil ui;
	
	/**
	 * Le plateau du jeu courant.
	 * 
	 * @see MoteurDeplacementAuto#getPlateau()
	 * @see MoteurDeplacementAuto#setPlateau(Plateau)
	 */
	private Plateau plateau;
	
	/**
	 * Constructeur d'un MoteurGravite
	 * 
	 * @param ui
	 * 			La fen�tre du jeu.
	 * @param plateau
	 * 			Le plateau du jeu courant.
	 */
	public MoteurGravite(ControleurFenetreAccueil ui, Plateau plateau) {
		setUi(ui);
		setPlateau(plateau);
	}
	
	/**
	 * Retourne la fen�tre du jeu.
	 * 
	 * @see ControleurFenetreAccueil
	 * 
	 * @see MoteurGravite#ui
	 */
	public ControleurFenetreAccueil getUi() {
		return ui;
	}

	/**
	 * Met � jour la fen�tre du jeu.
	 * 
	 * @see ControleurFenetreAccueil
	 * 
	 * @see MoteurGravite#ui
	 */
	public void setUi(ControleurFenetreAccueil ui) {
		this.ui = ui;
	}

	/**
	 * Retourne le plateau.
	 * 
	 * @see Plateau
	 * 
	 * @see MoteurGravite#plateau
	 */
	public Plateau getPlateau() {
		return plateau;
	}

	/**
	 * Met � jour le plateau.
	 * 
	 * @see Plateau
	 * 
	 * @see MoteurGravite#plateau
	 */
	public void setPlateau(Plateau plateau) {
		this.plateau = plateau;
	}

	/**
	  * <p>
	  * recevoirNotification est la fonction appel�e lorsqu'un changement dans l'�tat du jeu est d�tect�e.
	  * Ici la fonction r�sout le probl�me de la gravit� sur l'ensemble du plateau et met � jour son affichage.
	  * </p>
	  * 
	  * @throws Exception
	  * 
	  * @see Observateur
	  * 
	  * @see Plateau
	  * @see Plateau#appliquerGravite()
	  * 
	  * @see ControleurFenetreAccueil
	  * @see ControleurFenetreAccueil#dessinerGrille()
	  * 
	  * @see PanneauFooter
	  */
	@Override
	public void recevoirNotification(Observable observable) {
		try {
			plateau.appliquerGravite();
		} catch (Exception e) {
			ui.erreur(AlertType.INFORMATION, "Defaite !", "Vous avez perdu !");
			ui.loadMenu();
		}
		ui.dessinerGrille();
		ui.getPanneauFooter().getVie().notifier();
		ui.getPanneauFooter().getDiamant().notifier();
	}

}
