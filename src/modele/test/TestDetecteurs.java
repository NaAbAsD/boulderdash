package modele.test;

import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.Test;

import modele.cor.detecteurs.DetecteurDiamantRockford;
import modele.cor.detecteurs.DetecteurRocherRocherChute;
import modele.cor.detecteurs.DetecteurRocherRockford;
import modele.cor.detecteurs.DetecteurRockfordDiamant;
import modele.cor.detecteurs.DetecteurRockfordMonstre;
import modele.cor.detecteurs.DetecteurRockfordRocher;
import modele.cor.detecteurs.DetecteurRockfordTerre;
import modele.cor.detecteurs.DetecteurVide;
import modele.plateau.Plateau;
import modele.plateau.moteur.Deplacement;
import modele.plateau.objets.Acier;
import modele.plateau.objets.Diamant;
import modele.plateau.objets.Monstre;
import modele.plateau.objets.Objet;
import modele.plateau.objets.Rocher;
import modele.plateau.objets.Rockford;
import modele.plateau.objets.Terre;
import modele.plateau.objets.Vide;

public class TestDetecteurs {

	public void remplirTest(Plateau plateau) {
		Objet[][] grille = plateau.getGrille().getGrille();
		// Ligne 1
		grille[0][0] = new Rockford(0, 0);
		grille[0][5] = new Rockford(0, 5);
		grille[0][6] = new Monstre();
		grille[0][9] = new Rockford(0, 9);
		// Ligne 2
		grille[1][0] = new Acier();
		grille[1][1] = new Rockford(1, 1);
		grille[1][3] = new Rockford(1, 3);
		grille[1][4] = new Terre();
		grille[1][6] = new Rockford(1, 6);
		grille[1][7] = new Diamant();
		grille[1][8] = new Acier();
		// Ligne 4
		grille[3][0] = new Rockford(3, 0);
		grille[3][1] = new Rocher();
		grille[3][6] = new Monstre();
		grille[3][7] = new Rockford(3, 7);
		// Ligne 5
		grille[4][0] = new Rocher();
		grille[4][1] = new Rocher();
		grille[4][2] = new Rocher();
		grille[4][3] = new Rockford(4, 3);
		// Ligne 8
		grille[7][4] = new Rocher();
		grille[7][5] = new Rocher();
		grille[7][8] = new Diamant();
		// Ligne 9
		grille[8][1] = new Rocher();
		grille[8][3] = new Rocher();
		grille[8][7] = new Diamant();
		// Ligne 10
		grille[9][1] = new Rocher();
		grille[9][3] = new Rockford(9, 3);
		grille[9][4] = new Rockford(9, 4);
		grille[9][7] = new Rockford(9, 7);
		grille[9][8] = new Rockford(9, 8);
		// Le vide
		for (int i = 0; i < plateau.getHauteur(); i++) {
			for (int j = 0; j < plateau.getLargeur(); j++) {
				if (grille[i][j] == null) {
					plateau.setObjet(i, j, new Vide());
				}
			}
		}
	}
	
	@Test
    public void test() throws Exception {
		/*
		 * Set up du plateau
		 */
        Plateau plateau = new Plateau();
        remplirTest(plateau);
        System.out.println("Plateau de d�part :");
        plateau.printGrille();
        /*
		 * Ajout des d�tecteurs
		 */
        plateau.ajouterDetecteur(new DetecteurVide());
        plateau.ajouterDetecteur(new DetecteurRockfordMonstre());
        plateau.ajouterDetecteur(new DetecteurRockfordTerre());
        plateau.ajouterDetecteur(new DetecteurRockfordDiamant());
        plateau.ajouterDetecteur(new DetecteurRockfordRocher());
        plateau.ajouterDetecteur(new DetecteurRocherRockford());
        plateau.ajouterDetecteur(new DetecteurRocherRocherChute());
        plateau.ajouterDetecteur(new DetecteurDiamantRockford());
        /*
		 * S�quence de tests
		 */
        System.out.println("Interaction joueur-vide :");
        plateau.setJoueur((Rockford)plateau.getObjet(0, 0)); // On doit re set � chaque fois le joueur car la grille de test en contient plusieurs
        plateau.jouer(new Deplacement(plateau, plateau.getObjet(0, 0), 0, 0, Deplacement.DROITE));
        assertTrue(plateau.getObjet(0, 0) instanceof Vide, "La case (0, 0) n'est pas vide");
        assertTrue(plateau.getObjet(0, 1) instanceof Rockford, "La case (0, 1) ne contient pas Rockford");
        plateau.printGrille();
        
        System.out.println("Interaction joueur-acier :");
        plateau.setJoueur((Rockford)plateau.getObjet(1, 1));
        plateau.jouer(new Deplacement(plateau, plateau.getObjet(1, 1), 1, 1, Deplacement.GAUCHE));
        assertTrue(plateau.getObjet(1, 0) instanceof Acier, "La case (1, 0) contient pas d'acier");
        assertTrue(plateau.getObjet(1, 1) instanceof Rockford, "La case (1, 1) ne contient pas Rockford");
        plateau.printGrille();
        
        System.out.println("Interaction joueur-monstre :");
        plateau.setJoueur((Rockford)plateau.getObjet(0, 5));
        plateau.jouer(new Deplacement(plateau, plateau.getObjet(0, 5), 0, 5, Deplacement.DROITE));
        assertTrue(plateau.getObjet(0, 5) instanceof Vide, "La case (0, 5) n'est pas vide");
        assertTrue(plateau.getObjet(0, 6) instanceof Rockford, "La case (0, 6) ne contient pas Rockford");
        assertTrue(plateau.getJoueur().getVie() == 2, "Le joueur n'a pas perdu de vie");
        plateau.printGrille();
        
        System.out.println("Interaction joueur-terre :");
        plateau.setJoueur((Rockford)plateau.getObjet(1, 3));
        plateau.jouer(new Deplacement(plateau, plateau.getObjet(1, 3), 1, 3, Deplacement.DROITE));
        assertTrue(plateau.getObjet(1, 3) instanceof Vide, "La case (1, 3) n'est pas vide");
        assertTrue(plateau.getObjet(1, 4) instanceof Rockford, "La case (1, 4) ne contient pas Rockford");
        plateau.printGrille();

        System.out.println("Interaction joueur-diamant gagn� :");
        plateau.setJoueur((Rockford)plateau.getObjet(1, 6));
        plateau.jouer(new Deplacement(plateau, plateau.getObjet(1, 6), 1, 6, Deplacement.DROITE));
        assertTrue(plateau.getObjet(1, 6) instanceof Vide, "La case (1, 6) n'est pas vide");
        assertTrue(plateau.getObjet(1, 7) instanceof Rockford, "La case (1, 7) ne contient pas Rockford");
        assertTrue(plateau.getJoueur().getNbDiamants() == 1, "Le jouer n'a pas gagn� de diamant");
        plateau.printGrille();
        
        System.out.println("Interaction joueur-rocher pouss� :");
        plateau.setJoueur((Rockford)plateau.getObjet(3, 0));
        plateau.jouer(new Deplacement(plateau, plateau.getObjet(3, 0), 3, 0, Deplacement.DROITE));
        assertTrue(plateau.getObjet(3, 0) instanceof Vide, "La case (3, 0) n'est pas vide");
        assertTrue(plateau.getObjet(3, 1) instanceof Rockford, "La case (3, 1) ne contient pas Rockford");
        assertTrue(plateau.getObjet(3, 2) instanceof Rocher, "La case (3, 2) ne contient pas de rocher");
        plateau.printGrille();
        
        System.out.println("Interaction joueur-rocher bloqu� :");
        plateau.setJoueur((Rockford)plateau.getObjet(4, 3));
        plateau.jouer(new Deplacement(plateau, plateau.getObjet(4, 3), 4, 3, Deplacement.GAUCHE));
        assertTrue(plateau.getObjet(4, 2) instanceof Rocher, "La case (4, 2) ne contient pas de rocher");
        assertTrue(plateau.getObjet(4, 3) instanceof Rockford, "La case (4, 3) ne contient pas Rockford");
        plateau.printGrille();
        
        System.out.println("Gravit� rocher seul :");
        plateau.getObjet(7, 5).gravite(plateau, 7, 5);
        assertTrue(plateau.getObjet(7, 5) instanceof Vide, "La case (7, 5) n'est pas vide");
        assertTrue(plateau.getObjet(8, 5) instanceof Rocher, "La case (8, 5) ne contient pas de rocher");
        plateau.getObjet(8, 5).gravite(plateau, 8, 5);
        assertTrue(plateau.getObjet(8, 5) instanceof Vide, "La case (8, 5) n'est pas vide");
        assertTrue(plateau.getObjet(9, 5) instanceof Rocher, "La case (9, 5) ne contient pas de rocher");
        plateau.printGrille();
        
        System.out.println("Gravit� rocher-rockford avec d�g�ts :");
        plateau.setJoueur((Rockford)plateau.getObjet(9, 4));
        plateau.getObjet(7, 4).gravite(plateau, 7, 4);
        assertTrue(plateau.getObjet(7, 4) instanceof Vide, "La case (7, 4) n'est pas vide");
        assertTrue(plateau.getObjet(8, 4) instanceof Rocher, "La case (8, 4) ne contient pas de rocher");
        assertTrue(plateau.getObjet(9, 4) instanceof Rockford, "La case (9, 4) ne contient pas Rockford");
        plateau.getObjet(8, 4).gravite(plateau, 8, 4);
        assertTrue(plateau.getJoueur().getVie() == 2, "Le joueur n'a pas perdu de vie");
        plateau.printGrille();
        
        System.out.println("Gravit� rocher-rockford sans d�g�ts :");
        plateau.setJoueur((Rockford)plateau.getObjet(9, 3));
        plateau.getObjet(8, 3).gravite(plateau, 8, 3);
        assertTrue(plateau.getObjet(8, 3) instanceof Rocher, "La case (8, 3) ne contient pas de rocher");
        assertTrue(plateau.getObjet(9, 3) instanceof Rockford, "La case (9, 3) ne contient pas Rockford");
        assertTrue(plateau.getJoueur().getVie() == 3, "Le joueur a perdu une vie anormale");
        plateau.printGrille();
        
        System.out.println("Gravit� rocher-rocher :");
        plateau.getObjet(8, 1).gravite(plateau, 8, 1);
        assertTrue(plateau.getObjet(8, 1) instanceof Vide, "La case (8, 1) n'est pas vide");
        assertTrue(plateau.getObjet(8, 2) instanceof Rocher, "La case (8, 2) ne contient pas de rocher");
        plateau.getObjet(8, 2).gravite(plateau, 8, 2);
        assertTrue(plateau.getObjet(8, 2) instanceof Vide, "La case (8, 2) n'est pas vide");
        assertTrue(plateau.getObjet(9, 2) instanceof Rocher, "La case (9, 2) ne contient pas de rocher");
        plateau.printGrille();
        
		System.out.println("Gravit� diamant-rockford avec d�g�ts :");
		plateau.getObjet(7, 8).gravite(plateau, 7, 8);
		assertTrue(plateau.getObjet(7, 8) instanceof Vide, "La case (7, 8) n'est pas vide");
		assertTrue(plateau.getObjet(8, 8) instanceof Diamant, "La case (8, 8) ne contient pas de diamant");
		assertTrue(plateau.getObjet(9, 8) instanceof Rockford, "La case (9, 8) ne contient pas Rockford");
		plateau.getObjet(8, 8).gravite(plateau, 8, 8);
		assertTrue(plateau.getObjet(8, 8) instanceof Vide, "La case (8, 8) n'est pas vide");
		assertTrue(plateau.getObjet(9, 8) instanceof Rockford, "La case (9, 8) ne contient pas Rockford");
		assertTrue(plateau.getJoueur().getVie() == 2, "Le joueur n'a pas perdu de vie");
		assertTrue(plateau.getJoueur().getNbDiamants() == 1, "Le joueur n'a pas gagn� de diamant");
		plateau.printGrille();
        
        System.out.println("Gravit� diamant-rockford sans d�g�ts :");
        plateau.setJoueur((Rockford)plateau.getObjet(9, 7));
        plateau.getObjet(8, 7).gravite(plateau, 8, 7);
        assertTrue(plateau.getObjet(8, 7) instanceof Diamant, "La case (8, 7) ne contient pas de diamant");
        assertTrue(plateau.getObjet(9, 7) instanceof Rockford, "La case (9, 7) ne contient pas Rockford");
        assertTrue(plateau.getJoueur().getVie() == 3, "Le joueur a perdu une vie anormale");
        plateau.printGrille();
    }
	
}