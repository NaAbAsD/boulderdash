package modele.exceptions;

/**
 * <b>BoulderException est une classe qui �tend de la classe Exception de Java</b>
 */
@SuppressWarnings("serial")
public class BoulderException extends Exception {

	/**
	 * Constructeur de BoulderException
	 */
	public BoulderException() {
		super();
	}
	
	/**
	 * Constructeur de BoulderException comportant un String pour afficher la cause de l'exception
	 * 
	 * @param string
	 */
	public BoulderException(String string) {
		super(string);
	}

}
