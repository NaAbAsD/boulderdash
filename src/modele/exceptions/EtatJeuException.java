package modele.exceptions;

/**
 * <b>EtatJeuException est une classe qui �tend de la classe Exception de Java</b>
 * <p>
 * Cette classe est caract�ris�e par l'information suivante :
 * <ul>
 * <li>L'entier qui permet de connaitre l'etat de l'exception (Victoire ou Defaite).</li>
 * </ul>
 * </p>
 */
@SuppressWarnings("serial")
public class EtatJeuException extends Exception {

	/**
	 * <p>L'entier qui permet de connaitre l'etat de l'exception (Victoire ou Defaite).</p>
	 * 
	 * @see EtatJeuException#getEtat()
	 * @see EtatJeuException#setEtat(int)
	 */
	private int etat;
	
	public static final int DEFAITE = 0;
	public static final int VICTOIRE = 1;
	
	/**
	 * Constructeur de EtatJeuException en fonction de l'etat
	 * 
	 * @param etat
	 * 
	 * @see EtatJeuException#etat
	 */
	public EtatJeuException(int etat) {
		super();
		setEtat(etat);
	}
	
	/**
	 * Constructeur de EtatJeuException en fonction de l'etat et du message donnant la cause de l'exception
	 * 
	 * @param etat
	 * 
	 * @see EtatJeuException#etat
	 */
	public EtatJeuException(int etat, String string) {
		super(string);
		setEtat(etat);
	}

	/**
	 * Retourne un entier repr�sentant l'�tat actuel de l'exception
	 * 
	 * @return un entier
	 * 
	 * @see EtatJeuException#etat
	 */
	public int getEtat() {
		return etat;
	}

	/**
	 * Met � jour l'�tat actuel de l'exception
	 * 
	 * @param etat
	 * 
	 * @see EtatJeuException#etat
	 */
	public void setEtat(int etat) {
		this.etat = etat;
	}
	
}
