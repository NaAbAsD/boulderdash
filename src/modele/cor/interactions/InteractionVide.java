package modele.cor.interactions;

import modele.plateau.moteur.Deplacement;

/**
 * <b>InteractionVide est la classe qui �tend le class abstraite Interaction.</b>
 */
public class InteractionVide extends Interaction {

	/**
	 * Constructeur de la classe InteractionVide
	 * 
	 * @param dep
	 * 
	 * @see Interaction#Interaction(Deplacement)
	 */
	public InteractionVide(Deplacement dep) {
		super(dep);
	}

	/**
	 * Traitement de la classe InteractionVide
	 * 
	 * @see Interaction#traiter()
	 */
	@Override
	public void traiter() {
		getDeplacement().getPlateau().swap(getDeplacement());
	}

}