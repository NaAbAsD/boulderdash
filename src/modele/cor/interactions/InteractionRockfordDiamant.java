package modele.cor.interactions;

import modele.plateau.Plateau;
import modele.plateau.moteur.Deplacement;
import modele.plateau.objets.Vide;

/**
 * <b>InteractionRockfordDiamant est la classe qui �tend le class abstraite Interaction.</b>
 */
public class InteractionRockfordDiamant extends Interaction {

	/**
	 * Constructeur de la classe InteractionRockfordDiamant
	 * 
	 * @param dep
	 * 
	 * @see Interaction#Interaction(Deplacement)
	 */
	public InteractionRockfordDiamant(Deplacement dep) {
		super(dep);
	}

	/**
	 * Traitement de la classe InteractionRockfordDiamant
	 * 
	 * @see Interaction#traiter()
	 */
	@Override
	public void traiter() {
		Plateau plateau = getDeplacement().getPlateau();
		plateau.swap(getDeplacement());
		plateau.setObjet(getDeplacement().getlDep(), getDeplacement().getcDep(), new Vide());
		plateau.getJoueur().gagnerDiamant();
	}

}
