package modele.cor.interactions;

import modele.plateau.moteur.Deplacement;

/**
 * <b>Interaction est la classe abstraite servant de mod�le � toutes les autres classes interactions.</b>
 * <p>
 * Cette classe est caract�ris�e par l'information suivante :
 * <ul>
 * <li>Une instance de la classe Deplacement contenant toute les informations n�cessaire au fonctionnement de l'interaction.</li>
 * </ul>
 * </p>
 */
public abstract class Interaction {

	/**
	 * Le Deplacement utile au traitement de l'interaction d�tect�e.
	 * 
	 * @see Deplacement
	 * 
	 * @see Interaction#getDeplacement()
	 * @see Interaction#setDeplacement(Deplacement)
	 */
	private Deplacement dep;
	
	/**
     * Constructeur Interaction.
     * <p>
     * A la construction d'un objet Interaction, on set le Deplacement en param�tre � l'interaction d�tect�e.
     * </p>
     * 
     * @param dep
     *			L'instance de Deplacement n�cessaire � l'interaction
     * 
     * @see Interaction#dep
     * @see Interaction#setDeplacement(Deplacement)
     */
	public Interaction(Deplacement dep) {
		setDeplacement(dep);
	}
	
	/**
	 * Retourne le deplacement de l'interraction.
	 * 
	 * @return Une instance de Deplacement, celle-ci est associ�e � l'interaction d�tect�e.
	 * 
	 * @see Deplacement
	 */
	public Deplacement getDeplacement() {
		return dep;
	}

	/**
	 * Met � jour l'instance de Deplacement utilis�e par l'interaction
	 * 
	 * @param dep
	 * 
	 * @see Deplacement
	 */
	public void setDeplacement(Deplacement dep) {
		this.dep = dep;
	}

	/**
	 * Application de la r�gle du jeu correspondant � l'interaction entre les entit�s.
	 * 
	 * @throws Exception
	 */
	public abstract void traiter() throws Exception;
	
}
