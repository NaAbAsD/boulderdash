package modele.cor.interactions;

import modele.exceptions.EtatJeuException;
import modele.plateau.Plateau;
import modele.plateau.moteur.Deplacement;
import modele.plateau.objets.Vide;

/**
 * <b>InteractionRockfordMonstre est la classe qui �tend le class abstraite Interaction.</b>
 */
public class InteractionRockfordMonstre extends Interaction {

	/**
	 * Constructeur de la classe InteractionRockfordMonstre
	 * 
	 * @param dep
	 * 
	 * @see Interaction#Interaction(Deplacement)
	 */
	public InteractionRockfordMonstre(Deplacement dep) {
		super(dep);
	}

	/**
	 * Traitement de la classe InteractionRockfordMonstre
	 * 
	 * @see Interaction#traiter()
	 */
	@Override
	public void traiter() throws EtatJeuException {
		Plateau plateau = getDeplacement().getPlateau();
		plateau.swap(getDeplacement());
		plateau.setObjet(getDeplacement().getlDep(), getDeplacement().getcDep(), new Vide());
		plateau.getJoueur().perdreVie();
		if (plateau.getJoueur().getVie() <= 0) throw new EtatJeuException(EtatJeuException.DEFAITE);
	}

}
