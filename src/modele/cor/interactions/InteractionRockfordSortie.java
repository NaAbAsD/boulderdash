package modele.cor.interactions;

import modele.exceptions.BoulderException;
import modele.exceptions.EtatJeuException;
import modele.plateau.Plateau;
import modele.plateau.moteur.Deplacement;
import modele.plateau.objets.Vide;

/**
 * <b>InteractionRockfordSortie est la classe qui �tend le class abstraite Interaction.</b>
 */
public class InteractionRockfordSortie extends Interaction {

	/**
	 * Constructeur de la classe InteractionRockfordSortie
	 * 
	 * @param dep
	 * 
	 * @see Interaction#Interaction(Deplacement)
	 */
	public InteractionRockfordSortie(Deplacement dep) {
		super(dep);
	}

	/**
	 * Traitement de la classe InteractionRockfordSortie
	 * 
	 * @see Interaction#traiter()
	 */
	@Override
	public void traiter() throws Exception {
		Plateau plateau = getDeplacement().getPlateau();
		plateau.swap(getDeplacement());
		plateau.setObjet(getDeplacement().getlDep(), getDeplacement().getcDep(), new Vide());
		try {
			plateau.niveauSuivant();
		} catch (BoulderException e) {
			throw new EtatJeuException(EtatJeuException.VICTOIRE);
		}
	}

}
