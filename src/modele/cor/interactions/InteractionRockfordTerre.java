package modele.cor.interactions;

import modele.plateau.Plateau;
import modele.plateau.moteur.Deplacement;
import modele.plateau.objets.Vide;

/**
 * <b>InteractionRockfordTerre est la classe qui �tend le class abstraite Interaction.</b>
 */
public class InteractionRockfordTerre extends Interaction {

	/**
	 * Constructeur de la classe InteractionRockfordTerre
	 * 
	 * @param dep
	 * 
	 * @see Interaction#Interaction(Deplacement)
	 */
	public InteractionRockfordTerre(Deplacement dep) {
		super(dep);
	}

	/**
	 * Traitement de la classe InteractionRockfordTerre
	 * 
	 * @see Interaction#traiter()
	 */
	@Override
	public void traiter() {
		Plateau plateau = getDeplacement().getPlateau();
		plateau.swap(getDeplacement());
		plateau.setObjet(getDeplacement().getlDep(), getDeplacement().getcDep(), new Vide());
	}
	
}
