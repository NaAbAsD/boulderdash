package modele.cor.interactions;

import modele.plateau.Plateau;
import modele.plateau.moteur.Deplacement;

/**
 * <b>InteractionRockfordRocher est la classe qui �tend le class abstraite Interaction.</b>
 */
public class InteractionRockfordRocher extends Interaction {

	/**
	 * Constructeur de la classe InteractionRockfordRocher
	 * 
	 * @param dep
	 * 
	 * @see Interaction#Interaction(Deplacement)
	 */
	public InteractionRockfordRocher(Deplacement dep) {
		super(dep);
	}

	/**
	 * Traitement de la classe InteractionRockfordRocher
	 * 
	 * @see Interaction#traiter()
	 */
	@Override
	public void traiter() throws Exception {
		Plateau plateau = getDeplacement().getPlateau();
		Deplacement dep = getDeplacement();
		boolean rocherOk = plateau.jouer(new Deplacement(dep, plateau.getObjet(dep.getlArr(), dep.getcArr())));
		if (rocherOk) plateau.swap(dep);
	}

}
