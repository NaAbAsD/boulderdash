package modele.cor.interactions;

import modele.exceptions.EtatJeuException;
import modele.plateau.Plateau;
import modele.plateau.moteur.Deplacement;

/**
 * <b>InteractionRocherRockford est la classe qui �tend le class abstraite Interaction.</b>
 */
public class InteractionRocherRockford extends Interaction {

	/**
	 * Constructeur de la classe InteractionRocherRockford
	 * 
	 * @param dep
	 * 
	 * @see Interaction#Interaction(Deplacement)
	 */
	public InteractionRocherRockford(Deplacement dep) {
		super(dep);
	}

	/**
	 * Traitement de la classe InteractionRocherRockford
	 * 
	 * @see Interaction#traiter()
	 */
	@Override
	public void traiter() throws EtatJeuException {
		Deplacement dep = getDeplacement();
		Plateau plateau = dep.getPlateau();
		if (dep.getObjet().isEnChute()) {
			plateau.getJoueur().perdreVie();
			dep.getObjet().setEnChute(false);
			if (plateau.getJoueur().getVie() <= 0) throw new EtatJeuException(EtatJeuException.DEFAITE);
		}
	}

}
