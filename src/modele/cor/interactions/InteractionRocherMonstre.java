package modele.cor.interactions;

import modele.plateau.Plateau;
import modele.plateau.moteur.Deplacement;
import modele.plateau.objets.Diamant;
import modele.plateau.objets.Vide;

/**
 * <b>InteractionRocherMonstre est la classe qui �tend le class abstraite Interaction.</b>
 */
public class InteractionRocherMonstre extends Interaction {

	/**
	 * Constructeur de la classe InteractionRocherMonstre
	 * 
	 * @param dep
	 * 
	 * @see Interaction#Interaction(Deplacement)
	 */
	public InteractionRocherMonstre(Deplacement dep) {
		super(dep);
	}

	/**
	 * Traitement de la classe InteractionRocherMonstre
	 * 
	 * @see Interaction#traiter()
	 */
	@Override
	public void traiter() {
		Deplacement dep = getDeplacement();
		Plateau plateau = dep.getPlateau();
		plateau.setObjet(dep.getlDep(), dep.getcDep(), new Vide());
		plateau.setObjet(dep.getlArr(), dep.getcArr(), new Diamant());
	}

}
