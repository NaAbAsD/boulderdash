package modele.cor.interactions;

import modele.exceptions.EtatJeuException;
import modele.plateau.Plateau;
import modele.plateau.moteur.Deplacement;
import modele.plateau.objets.Vide;

/**
 * <b>InteractionDiamantRockford est la classe qui �tend le class abstraite Interaction.</b>
 */
public class InteractionDiamantRockford extends Interaction {

	/**
	 * Constructeur de la classe InteractionDiamantRockford
	 * 
	 * @param dep
	 * 
	 * @see Interaction#Interaction(Deplacement)
	 */
	public InteractionDiamantRockford(Deplacement dep) {
		super(dep);
	}

	/**
	 * Traitement de la classe Interaction DiamantRockford
	 * 
	 * @see Interaction#traiter()
	 */
	@Override
	public void traiter() throws EtatJeuException {
		Deplacement dep = getDeplacement();
		Plateau plateau = getDeplacement().getPlateau();
		if (dep.getObjet().isEnChute()) {
			plateau.getJoueur().gagnerDiamant();
			plateau.getJoueur().perdreVie();
			plateau.setObjet(dep.getlDep(), dep.getcDep(), new Vide());
			if (plateau.getJoueur().getVie() <= 0) throw new EtatJeuException(EtatJeuException.DEFAITE);
		}
	}
	
}
