package modele.cor.interactions;

import modele.exceptions.EtatJeuException;
import modele.plateau.Plateau;
import modele.plateau.moteur.Deplacement;
import modele.plateau.objets.Vide;

/**
 * <b>InteractionMonstreRockford est la classe qui �tend le class abstraite Interaction.</b>
 */
public class InteractionMonstreRockford extends Interaction {

	/**
	 * Constructeur de la classe InteractionMonstreRockford
	 * 
	 * @param dep
	 * 
	 * @see Interaction#Interaction(Deplacement)
	 */
	public InteractionMonstreRockford(Deplacement dep) {
		super(dep);
	}

	/**
	 * Traitement de la classe InteractionMonstreRockford
	 * 
	 * @see Interaction#traiter()
	 */
	@Override
	public void traiter() throws EtatJeuException {
		Plateau plateau = getDeplacement().getPlateau();
		plateau.getJoueur().perdreVie();
		plateau.setObjet(getDeplacement().getlDep(), getDeplacement().getcDep(), new Vide());
		if (plateau.getJoueur().getVie() <= 0) throw new EtatJeuException(EtatJeuException.DEFAITE);
	}

}
