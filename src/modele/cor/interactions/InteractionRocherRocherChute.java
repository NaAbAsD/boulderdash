package modele.cor.interactions;

import modele.plateau.Plateau;
import modele.plateau.moteur.Deplacement;

/**
 * <b>InteractionRocherRocherChute est la classe qui �tend le class abstraite Interaction.</b>
 */
public class InteractionRocherRocherChute extends Interaction {

	/**
	 * Constructeur de la classe InteractionRocherRocherChute
	 * 
	 * @param dep
	 * 
	 * @see Interaction#Interaction(Deplacement)
	 */
	public InteractionRocherRocherChute(Deplacement dep) {
		super(dep);
	}

	/**
	 * Traitement de la classe InteractionRocherRocherChute
	 * 
	 * @see Interaction#traiter()
	 */
	@Override
	public void traiter() throws Exception {
		Deplacement dep = getDeplacement();
		Plateau plateau = getDeplacement().getPlateau();
		boolean glisser = plateau.jouer(new Deplacement(plateau, dep.getObjet(), dep.getlDep(), dep.getcDep(), Deplacement.DROITE));
		if (glisser) plateau.swap(dep);
	}

}
