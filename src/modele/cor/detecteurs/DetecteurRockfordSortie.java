package modele.cor.detecteurs;

import modele.cor.interactions.InteractionRockfordSortie;
import modele.plateau.moteur.Deplacement;
import modele.plateau.objets.Rockford;
import modele.plateau.objets.Sortie;

/**
 * <b>DetecteurRockfordSortie est la classe qui �tend la classe abstraite DetecteurInteractions.</b>
 */
public class DetecteurRockfordSortie extends DetecteurInteractions {

	/**
	 * Une instance de InteractionRockfordSortie afin de la retourner si elle est detect�e
	 * 
	 * @see InteractionRockfordSortie
	 */
	private InteractionRockfordSortie interaction;
	
	/**
	 * Constructeur de DetecteurRockfordSortie
	 */
	public DetecteurRockfordSortie() {}
	
	/**
	 * Retourne l'instance de InteractionRockfordSortie
	 * 
	 * @return une instance de InteractionRockfordSortie
	 * 
	 * @see InteractionRockfordSortie
	 */
	public InteractionRockfordSortie getInteraction() {
		return interaction;
	}
	
	/**
	 * Met � jour l'interaction correspondante au detecteur
	 * 
	 * @param interaction
	 * 
	 * @see InteractionRockfordSortie
	 */
	public void setInteraction(InteractionRockfordSortie interaction) {
		this.interaction = interaction;
	}

	/**
	 * Retourne un booleen, vrai si le Deplacement correspond � la r�gle du DetecteurRockfordSortie
	 * 
	 * @see InteractionRockfordSortie
	 * @see Deplacement
	 * 
	 * @see DetecteurInteractions
	 * @see DetecteurInteractions#estDetectee(Deplacement)
	 */
	@Override
	public boolean estDetectee(Deplacement d) {
		if (d.getObjet() instanceof Rockford && d.getPlateau().getGrille().getObjet(d.getlArr(), d.getcArr()) instanceof Sortie) {
			setInteraction(new InteractionRockfordSortie(d));
			return true;
		}
		return false;
	}

	/**
	 * Retourne une instance de InteractionRockfordSortie
	 * 
	 * @return une instance de l'interaction
	 * 
	 * @see InteractionRockfordSortie
	 * 
	 * @see DetecteurInteractions
	 * @see DetecteurInteractions#extraire()
	 */
	@Override
	public InteractionRockfordSortie extraire() {
		return interaction;
	}

}
