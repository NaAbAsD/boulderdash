package modele.cor.detecteurs;

import modele.cor.interactions.InteractionRocherMonstre;
import modele.plateau.objets.Rocher;
import modele.plateau.moteur.Deplacement;
import modele.plateau.objets.Monstre;

/**
 * <b>DetecteurRocherMonstre est la classe qui �tend la classe abstraite DetecteurInteractions.</b>
 */
public class DetecteurRocherMonstre extends DetecteurInteractions {

	/**
	 * Une instance de InteractionRocherMonstre afin de la retourner si elle est detect�e
	 * 
	 * @see InteractionRocherMonstre
	 */
	private InteractionRocherMonstre interaction;
	
	/**
	 * Constructeur de DetecteurRocherMonstre
	 */
	public DetecteurRocherMonstre() {}
	
	/**
	 * Retourne l'instance de InteractionRocherMonstre
	 * 
	 * @return une instance de InteractionRocherMonstre
	 * 
	 * @see InteractionRocherMonstre
	 */
	public InteractionRocherMonstre getInteraction() {
		return interaction;
	}
	
	/**
	 * Met � jour l'interaction correspondante au detecteur
	 * 
	 * @param interaction
	 * 
	 * @see InteractionRocherMonstre
	 */
	public void setInteraction(InteractionRocherMonstre interaction) {
		this.interaction = interaction;
	}

	/**
	 * Retourne un booleen, vrai si le Deplacement correspond � la r�gle du DetecteurRocherMonstre
	 * 
	 * @see InteractionRocherMonstre
	 * @see Deplacement
	 * 
	 * @see DetecteurInteractions
	 * @see DetecteurInteractions#estDetectee(Deplacement)
	 */
	@Override
	public boolean estDetectee(Deplacement d) {
		if (d.getObjet() instanceof Rocher && d.getPlateau().getObjet(d.getlArr(), d.getcArr()) instanceof Monstre) {
			setInteraction(new InteractionRocherMonstre(d));
			return true;
		}
		return false;
	}

	/**
	 * Retourne une instance de InteractionRocherMonstre
	 * 
	 * @return une instance de l'interaction
	 * 
	 * @see InteractionRocherMonstre
	 * 
	 * @see DetecteurInteractions
	 * @see DetecteurInteractions#extraire()
	 */
	@Override
	public InteractionRocherMonstre extraire() {
		return interaction;
	}

}
