package modele.cor.detecteurs;

import modele.cor.interactions.InteractionRockfordTerre;
import modele.plateau.moteur.Deplacement;
import modele.plateau.objets.Rockford;
import modele.plateau.objets.Terre;

/**
 * <b>DetecteurRockfordTerre est la classe qui �tend la classe abstraite DetecteurInteractions.</b>
 */
public class DetecteurRockfordTerre extends DetecteurInteractions {

	/**
	 * Une instance de InteractionRockfordTerre afin de la retourner si elle est detect�e
	 * 
	 * @see InteractionRockfordTerre
	 */
	private InteractionRockfordTerre interaction;
	
	/**
	 * Constructeur de DetecteurRockfordTerre
	 */
	public DetecteurRockfordTerre() {}
	
	/**
	 * Retourne l'instance de InteractionRockfordTerre
	 * 
	 * @return une instance de InteractionRockfordTerre
	 * 
	 * @see InteractionRockfordTerre
	 */
	public InteractionRockfordTerre getInteraction() {
		return interaction;
	}
	
	/**
	 * Met � jour l'interaction correspondante au detecteur
	 * 
	 * @param interaction
	 * 
	 * @see InteractionRockfordTerre
	 */
	public void setInteraction(InteractionRockfordTerre interaction) {
		this.interaction = interaction;
	}

	/**
	 * Retourne un booleen, vrai si le Deplacement correspond � la r�gle du DetecteurRockfordTerre
	 * 
	 * @see InteractionRockfordTerre
	 * @see Deplacement
	 * 
	 * @see DetecteurInteractions
	 * @see DetecteurInteractions#estDetectee(Deplacement)
	 */
	@Override
	public boolean estDetectee(Deplacement d) {
		if (d.getObjet() instanceof Rockford && d.getPlateau().getObjet(d.getlArr(), d.getcArr()) instanceof Terre) {
			setInteraction(new InteractionRockfordTerre(d));
			return true;
		}
		return false;
	}

	/**
	 * Retourne une instance de InteractionRockfordTerre
	 * 
	 * @return une instance de l'interaction
	 * 
	 * @see InteractionRockfordTerre
	 * 
	 * @see DetecteurInteractions
	 * @see DetecteurInteractions#extraire()
	 */
	@Override
	public InteractionRockfordTerre extraire() {
		return interaction;
	}
	
}
