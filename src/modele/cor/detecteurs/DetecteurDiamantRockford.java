package modele.cor.detecteurs;

import modele.cor.interactions.InteractionDiamantRockford;
import modele.plateau.moteur.Deplacement;
import modele.plateau.objets.Diamant;
import modele.plateau.objets.Rockford;

/**
 * <b>DetecteurDiamantRockford est la classe qui �tend la classe abstraite DetecteurInteractions.</b>
 */
public class DetecteurDiamantRockford extends DetecteurInteractions {

	/**
	 * Une instance de InteractionDiamantRockford afin de la retourner si elle est detect�e
	 * 
	 * @see InteractionDiamantRockford
	 */
	private InteractionDiamantRockford interaction;
	
	/**
	 * Constructeur de DetecteurDiamantRockford
	 */
	public DetecteurDiamantRockford() {}
	
	/**
	 * Retourne l'instance de InteractionDiamantRockford
	 * 
	 * @return une instance de InteractionDiamantRockford
	 * 
	 * @see InteractionDiamantRockford
	 */
	public InteractionDiamantRockford getInteraction() {
		return interaction;
	}
	
	/**
	 * Met � jour l'interaction correspondante au detecteur
	 * 
	 * @param interaction
	 * 
	 * @see InteractionDiamantRockford
	 */
	public void setInteraction(InteractionDiamantRockford interaction) {
		this.interaction = interaction;
	}

	/**
	 * Retourne un booleen, vrai si le Deplacement correspond � la r�gle du DetecteurDiamantRockford
	 * 
	 * @see InteractionDiamantRockford
	 * @see Deplacement
	 * 
	 * @see DetecteurInteractions
	 * @see DetecteurInteractions#estDetectee(Deplacement)
	 */
	@Override
	public boolean estDetectee(Deplacement d) {
		if (d.getObjet() instanceof Diamant && d.getPlateau().getObjet(d.getlArr(), d.getcArr()) instanceof Rockford) {
			setInteraction(new InteractionDiamantRockford(d));
			return true;
		}
		return false;
	}

	/**
	 * Retourne une instance de InteractionDiamantRockford
	 * 
	 * @return une instance de l'interaction
	 * 
	 * @see InteractionDiamantRockford
	 * 
	 * @see DetecteurInteractions
	 * @see DetecteurInteractions#extraire()
	 */
	@Override
	public InteractionDiamantRockford extraire() {
		return interaction;
	}

}
