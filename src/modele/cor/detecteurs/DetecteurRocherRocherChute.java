package modele.cor.detecteurs;

import modele.cor.interactions.InteractionRocherRocherChute;
import modele.plateau.Plateau;
import modele.plateau.moteur.Deplacement;
import modele.plateau.objets.Rocher;
import modele.plateau.objets.Vide;

/**
 * <b>DetecteurRocherRocherChute est la classe qui �tend la classe abstraite DetecteurInteractions.</b>
 */
public class DetecteurRocherRocherChute extends DetecteurInteractions {

	/**
	 * Une instance de InteractionRocherRocherChute afin de la retourner si elle est detect�e
	 * 
	 * @see InteractionRocherRocherChute
	 */
	private InteractionRocherRocherChute interaction;
	
	/**
	 * Constructeur de DetecteurRocherRocherChute
	 */
	public DetecteurRocherRocherChute() {}
	
	/**
	 * Retourne l'instance de InteractionRocherRocherChute
	 * 
	 * @return une instance de InteractionRocherRocherChute
	 * 
	 * @see InteractionRocherRocherChute
	 */
	public InteractionRocherRocherChute getInteraction() {
		return interaction;
	}
	
	/**
	 * Met � jour l'interaction correspondante au detecteur
	 * 
	 * @param interaction
	 * 
	 * @see InteractionRocherRocherChute
	 */
	public void setInteraction(InteractionRocherRocherChute interaction) {
		this.interaction = interaction;
	}

	/**
	 * Retourne un booleen, vrai si le Deplacement correspond � la r�gle du DetecteurRocherRocherChute
	 * 
	 * @see InteractionRocherRocherChute
	 * @see Deplacement
	 * 
	 * @see DetecteurInteractions
	 * @see DetecteurInteractions#estDetectee(Deplacement)
	 */
	@Override
	public boolean estDetectee(Deplacement d) {
		Plateau plateau = d.getPlateau();
		/* 
		 * Les 3 derni�res conditions sont pratiques pour v�rifier le bord du plateau.
		 * Ainsi, si le rocher ne peux pas glisser, nous ne pouvons pas valider cette condition.
		 * Alors, on retourne false et l'appel "rocherOk" de l'int�ration Rockford-Rocher n'a pas lieu
		 */
		if (d.getObjet() instanceof Rocher && plateau.getObjet(d.getlArr(), d.getcArr()) instanceof Rocher &&
				d.getDir() == Deplacement.BAS && d.getcDep() + 1 < plateau.getHauteur()
				&& plateau.getObjet(d.getlDep(), d.getcDep() + 1) instanceof Vide) {
			setInteraction(new InteractionRocherRocherChute(d));
			return true;
		}
		return false;
	}

	/**
	 * Retourne une instance de InteractionRocherRocherChute
	 * 
	 * @return une instance de l'interaction
	 * 
	 * @see InteractionRocherRocherChute
	 * 
	 * @see DetecteurInteractions
	 * @see DetecteurInteractions#extraire()
	 */
	@Override
	public InteractionRocherRocherChute extraire() {
		return interaction;
	}

}
