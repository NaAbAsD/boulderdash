package modele.cor.detecteurs;

import modele.cor.interactions.InteractionMonstreRockford;
import modele.plateau.moteur.Deplacement;
import modele.plateau.objets.Monstre;
import modele.plateau.objets.Rockford;

/**
 * <b>DetecteurMonstreRockford est la classe qui �tend la classe abstraite DetecteurInteractions.</b>
 */
public class DetecteurMonstreRockford extends DetecteurInteractions {

	/**
	 * Une instance de InteractionMonstreRockford afin de la retourner si elle est detect�e
	 * 
	 * @see InteractionMonstreRockford
	 */
	private InteractionMonstreRockford interaction;
	
	/**
	 * Constructeur de DetecteurMonstreRockford
	 */
	public DetecteurMonstreRockford() {}
	
	/**
	 * Retourne l'instance de InteractionMonstreRockford
	 * 
	 * @return une instance de InteractionMonstreRockford
	 * 
	 * @see InteractionMonstreRockford
	 */
	public InteractionMonstreRockford getInteraction() {
		return interaction;
	}
	
	/**
	 * Met � jour l'interaction correspondante au detecteur
	 * 
	 * @param interaction
	 * 
	 * @see InteractionMonstreRockford
	 */
	public void setInteraction(InteractionMonstreRockford interaction) {
		this.interaction = interaction;
	}

	/**
	 * Retourne un booleen, vrai si le Deplacement correspond � la r�gle du DetecteurMonstreRockford
	 * 
	 * @see InteractionMonstreRockford
	 * @see Deplacement
	 * 
	 * @see DetecteurInteractions
	 * @see DetecteurInteractions#estDetectee(Deplacement)
	 */
	@Override
	public boolean estDetectee(Deplacement d) {
		if (d.getObjet() instanceof Monstre && d.getPlateau().getObjet(d.getlArr(), d.getcArr()) instanceof Rockford) {
			setInteraction(new InteractionMonstreRockford(d));
			return true;
		}
		return false;
	}

	/**
	 * Retourne une instance de InteractionMonstreRockford
	 * 
	 * @return une instance de l'interaction
	 * 
	 * @see InteractionMonstreRockford
	 * 
	 * @see DetecteurInteractions
	 * @see DetecteurInteractions#extraire()
	 */
	@Override
	public InteractionMonstreRockford extraire() {
		return interaction;
	}

}
