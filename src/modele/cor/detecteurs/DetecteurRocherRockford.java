package modele.cor.detecteurs;

import modele.cor.interactions.InteractionRocherRockford;
import modele.plateau.moteur.Deplacement;
import modele.plateau.objets.Rocher;
import modele.plateau.objets.Rockford;

/**
 * <b>DetecteurRocherRockford est la classe qui �tend la classe abstraite DetecteurInteractions.</b>
 */
public class DetecteurRocherRockford extends DetecteurInteractions {

	/**
	 * Une instance de InteractionRocherRockford afin de la retourner si elle est detect�e
	 * 
	 * @see InteractionRocherRockford
	 */
	private InteractionRocherRockford interaction;
	
	/**
	 * Constructeur de DetecteurRocherRockford
	 */
	public DetecteurRocherRockford() {}
	
	/**
	 * Retourne l'instance de InteractionRocherRockford
	 * 
	 * @return une instance de InteractionRocherRockford
	 * 
	 * @see InteractionRocherRockford
	 */
	public InteractionRocherRockford getInteraction() {
		return interaction;
	}
	
	/**
	 * Met � jour l'interaction correspondante au detecteur
	 * 
	 * @param interaction
	 * 
	 * @see InteractionRocherRockford
	 */
	public void setInteraction(InteractionRocherRockford interaction) {
		this.interaction = interaction;
	}

	/**
	 * Retourne un booleen, vrai si le Deplacement correspond � la r�gle du DetecteurRocherRockford
	 * 
	 * @see InteractionRocherRockford
	 * @see Deplacement
	 * 
	 * @see DetecteurInteractions
	 * @see DetecteurInteractions#estDetectee(Deplacement)
	 */
	@Override
	public boolean estDetectee(Deplacement d) {
		if (d.getObjet() instanceof Rocher && d.getPlateau().getObjet(d.getlArr(), d.getcArr()) instanceof Rockford) {
			setInteraction(new InteractionRocherRockford(d));
			return true;
		}
		return false;
	}

	/**
	 * Retourne une instance de InteractionRocherRockford
	 * 
	 * @return une instance de l'interaction
	 * 
	 * @see InteractionRocherRockford
	 * 
	 * @see DetecteurInteractions
	 * @see DetecteurInteractions#extraire()
	 */
	@Override
	public InteractionRocherRockford extraire() {
		return interaction;
	}

}
