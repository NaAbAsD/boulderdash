package modele.cor.detecteurs;

import modele.cor.interactions.Interaction;
import modele.plateau.moteur.Deplacement;

/**
 * <b>DetecteurInteractions est la classe abstraite servant de mod�le � toutes les autres classes Detecteurs.</b>
 * <p>
 * Cette classe est caract�ris�e par l'information suivante :
 * <ul>
 * <li>Une instance de la classe DetecteurInteractions contenant les prochains d�tecteurs (de fa�on r�cursive)
 * � appeler si l'interaction n'est pas detect�e.</li>
 * </ul>
 * </p>
 */
public abstract class DetecteurInteractions {
	
	/**
	 * Instance de DetecteurInteractions qui est le prochain d�tecteur � appeler si celui la ne d�tecte pas l'interaction
	 * 
	 * @see DetecteurInteractions#getSuivant()
	 * @see DetecteurInteractions#setSuivant()
	 */
	private DetecteurInteractions suivant = null;
	
	/**
	 * Constructeur de DetecteurInteractions
	 */
	public DetecteurInteractions() {}
	
	/**
	 * Constructeur de DetecteurInteractions
	 *  <p>
     *  A la construction d'un objet DetecteurInteractions, on set les DetecteurInteractions en param�tre.
     * </p>
     * 
	 * @param s
	 * 			Le prochain/les prochains d�tecteurs � appeler de mani�re r�cursive.
	 * 
	 * @see DetecteurInteractions
	 * @see DetecteurInteractions#setSuivant(DetecteurInteractions)
	 */
	public DetecteurInteractions(DetecteurInteractions s) {
		setSuivant(s);
	}
	
	/**
	 * Retourne les d�tecteurs � appeler apr�s celui ci.
	 * 
	 * @return Le/Les d�tecteurs suivant.
	 * 
	 * see DetecteurInteractions
	 */
	public DetecteurInteractions getSuivant() {
		return suivant;
	}

	/**
	 * Met � jour la liste des d�tecteurs � appeler.
	 * 
	 * @param suivant
	 * 
	 * see DetecteurInteractions
	 */
	public void setSuivant(DetecteurInteractions suivant) {
		this.suivant = suivant;
	}

	/**
	 * Retourne un bool�en qui permet de savoir si on a d�tect� l'interaction correspondante au d�tecteur.
	 * 
	 * @param d
	 * 			Une instance de Deplacement utilis� pour v�rifier on d�tecte l'interaction sur les positions de l'instance 
	 * 
	 * @return Un bool�en permettant de savoir si l'interaction est d�tect�e.
	 * 
	 * @see Deplacement
	 */
	public abstract boolean estDetectee(Deplacement d);
	
	/**
	 * Retourne l'interaction d�tect� par le d�tecteur correspondant
	 * 
	 * @return Une instance d'interaction correspondant � l'interaction reconnu par le d�tecteur
	 * 
	 * @see Interaction
	 */
	public abstract Interaction extraire();
	
	/**
	 * <p>
	 * Retourne une interaction si le Detecteur d�tecte l'interaction correspondante, 
	 * sinon il rappelle de fa�on r�cursive la fonction avec le d�tecteur suivant !
	 * </p>
	 * 
	 * @param d
	 * 			Une instance de Deplacement n�cessaire � l'appel de estDetectee
	 * 
	 * @return Une instance de Interaction correspondant � l'interaction retourner par la extraire()
	 * 
	 * @see Interaction
	 * 
	 * @see DetecteurInteractions#suivant
	 * @see DetecteurInteractions#estDetectee(Deplacement)
	 * @see DetecteurInteractions#extraire()
	 */
	public Interaction detecter(Deplacement d) {
		if (estDetectee(d)) {
			return extraire();
		} else if (suivant != null) {
			return suivant.detecter(d);
		} else {
			return null;
		}
	}

}
