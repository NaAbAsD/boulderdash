package modele.cor.detecteurs;

import modele.cor.interactions.InteractionVide;
import modele.plateau.moteur.Deplacement;
import modele.plateau.objets.Vide;

/**
 * <b>DetecteurVide est la classe qui �tend la classe abstraite DetecteurInteractions.</b>
 */
public class DetecteurVide extends DetecteurInteractions {

	/**
	 * Une instance de InteractionVide afin de la retourner si elle est detect�e
	 * 
	 * @see InteractionVide
	 */
	private InteractionVide interaction;
	
	/**
	 * Constructeur de DetecteurVide
	 */
	public DetecteurVide() {}
	
	/**
	 * Retourne l'instance de InteractionVide
	 * 
	 * @return une instance de InteractionVide
	 * 
	 * @see InteractionVide
	 */
	public InteractionVide getInteraction() {
		return interaction;
	}
	
	/**
	 * Met � jour l'interaction correspondante au detecteur
	 * 
	 * @param interaction
	 * 
	 * @see InteractionVide
	 */
	public void setInteraction(InteractionVide interaction) {
		this.interaction = interaction;
	}

	/**
	 * Retourne un booleen, vrai si le Deplacement correspond � la r�gle du DetecteurVide
	 * 
	 * @see InteractionVide
	 * @see Deplacement
	 * 
	 * @see DetecteurInteractions
	 * @see DetecteurInteractions#estDetectee(Deplacement)
	 */
	@Override
	public boolean estDetectee(Deplacement d) {
		if (d.getPlateau().getObjet(d.getlArr(), d.getcArr()) instanceof Vide) {
			setInteraction(new InteractionVide(d));
			return true;
		}
		return false;
	}

	/**
	 * Retourne une instance de InteractionVide
	 * 
	 * @return une instance de l'interaction
	 * 
	 * @see InteractionVide
	 * 
	 * @see DetecteurInteractions
	 * @see DetecteurInteractions#extraire()
	 */
	@Override
	public InteractionVide extraire() {
		return interaction;
	}

}
