package modele.cor.detecteurs;

import modele.cor.interactions.InteractionRockfordDiamant;
import modele.plateau.objets.Rockford;
import modele.plateau.moteur.Deplacement;
import modele.plateau.objets.Diamant;

/**
 * <b>DetecteurRockfordDiamant est la classe qui �tend la classe abstraite DetecteurInteractions.</b>
 */
public class DetecteurRockfordDiamant extends DetecteurInteractions {

	/**
	 * Une instance de InteractionRockfordDiamant afin de la retourner si elle est detect�e
	 * 
	 * @see InteractionRockfordDiamant
	 */
	private InteractionRockfordDiamant interaction;
	
	/**
	 * Constructeur de DetecteurRockfordDiamant
	 */
	public DetecteurRockfordDiamant() {}
	
	/**
	 * Retourne l'instance de InteractionRockfordDiamant
	 * 
	 * @return une instance de InteractionRockfordDiamant
	 * 
	 * @see InteractionRockfordDiamant
	 */
	public InteractionRockfordDiamant getInteraction() {
		return interaction;
	}
	
	/**
	 * Met � jour l'interaction correspondante au detecteur
	 * 
	 * @param interaction
	 * 
	 * @see InteractionRockfordDiamant
	 */
	public void setInteraction(InteractionRockfordDiamant interaction) {
		this.interaction = interaction;
	}

	/**
	 * Retourne un booleen, vrai si le Deplacement correspond � la r�gle du DetecteurRockfordDiamant
	 * 
	 * @see InteractionRockfordDiamant
	 * @see Deplacement
	 * 
	 * @see DetecteurInteractions
	 * @see DetecteurInteractions#estDetectee(Deplacement)
	 */
	@Override
	public boolean estDetectee(Deplacement d) {
		if (d.getObjet() instanceof Rockford && d.getPlateau().getObjet(d.getlArr(), d.getcArr()) instanceof Diamant) {
			setInteraction(new InteractionRockfordDiamant(d));
			return true;
		}
		return false;
	}

	/**
	 * Retourne une instance de InteractionRockfordDiamant
	 * 
	 * @return une instance de l'interaction
	 * 
	 * @see InteractionRockfordDiamant
	 * 
	 * @see DetecteurInteractions
	 * @see DetecteurInteractions#extraire()
	 */
	@Override
	public InteractionRockfordDiamant extraire() {
		return interaction;
	}

}
