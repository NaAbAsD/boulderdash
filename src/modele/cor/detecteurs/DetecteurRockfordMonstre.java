package modele.cor.detecteurs;

import modele.cor.interactions.InteractionRockfordMonstre;
import modele.plateau.objets.Rockford;
import modele.plateau.moteur.Deplacement;
import modele.plateau.objets.Monstre;

/**
 * <b>DetecteurRockfordMonstre est la classe qui �tend la classe abstraite DetecteurInteractions.</b>
 */
public class DetecteurRockfordMonstre extends DetecteurInteractions {

	/**
	 * Une instance de InteractionRockfordMonstre afin de la retourner si elle est detect�e
	 * 
	 * @see InteractionRockfordMonstre
	 */
	private InteractionRockfordMonstre interaction;
	
	/**
	 * Constructeur de DetecteurRockfordMonstre
	 */
	public DetecteurRockfordMonstre() {}
	
	/**
	 * Retourne l'instance de InteractionRockfordMonstre
	 * 
	 * @return une instance de InteractionRockfordMonstre
	 * 
	 * @see InteractionRockfordMonstre
	 */
	public InteractionRockfordMonstre getInteraction() {
		return interaction;
	}
	
	/**
	 * Met � jour l'interaction correspondante au detecteur
	 * 
	 * @param interaction
	 * 
	 * @see InteractionRockfordMonstre
	 */
	public void setInteraction(InteractionRockfordMonstre interaction) {
		this.interaction = interaction;
	}

	/**
	 * Retourne un booleen, vrai si le Deplacement correspond � la r�gle du DetecteurRockfordMonstre
	 * 
	 * @see InteractionRockfordMonstre
	 * @see Deplacement
	 * 
	 * @see DetecteurInteractions
	 * @see DetecteurInteractions#estDetectee(Deplacement)
	 */
	@Override
	public boolean estDetectee(Deplacement d) {
		if (d.getObjet() instanceof Rockford && d.getPlateau().getObjet(d.getlArr(), d.getcArr()) instanceof Monstre) {
			setInteraction(new InteractionRockfordMonstre(d));
			return true;
		}
		return false;
	}

	/**
	 * Retourne une instance de InteractionRockfordMonstre
	 * 
	 * @return une instance de l'interaction
	 * 
	 * @see InteractionRockfordMonstre
	 * 
	 * @see DetecteurInteractions
	 * @see DetecteurInteractions#extraire()
	 */
	@Override
	public InteractionRockfordMonstre extraire() {
		return interaction;
	}

}
