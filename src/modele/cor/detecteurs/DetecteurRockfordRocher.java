package modele.cor.detecteurs;

import modele.cor.interactions.InteractionRockfordRocher;
import modele.plateau.objets.Rockford;
import modele.plateau.moteur.Deplacement;
import modele.plateau.objets.Rocher;

/**
 * <b>DetecteurRockfordRocher est la classe qui �tend la classe abstraite DetecteurInteractions.</b>
 */
public class DetecteurRockfordRocher extends DetecteurInteractions {

	/**
	 * Une instance de InteractionRockfordRocher afin de la retourner si elle est detect�e
	 * 
	 * @see InteractionRockfordRocher
	 */
	private InteractionRockfordRocher interaction;
	
	/**
	 * Constructeur de DetecteurRockfordRocher
	 */
	public DetecteurRockfordRocher() {}
	
	/**
	 * Retourne l'instance de InteractionRockfordRocher
	 * 
	 * @return une instance de InteractionRockfordRocher
	 * 
	 * @see InteractionRockfordRocher
	 */
	public InteractionRockfordRocher getInteraction() {
		return interaction;
	}
	
	/**
	 * Met � jour l'interaction correspondante au detecteur
	 * 
	 * @param interaction
	 * 
	 * @see InteractionRockfordRocher
	 */
	public void setInteraction(InteractionRockfordRocher interaction) {
		this.interaction = interaction;
	}

	/**
	 * Retourne un booleen, vrai si le Deplacement correspond � la r�gle du DetecteurRockfordRocher
	 * 
	 * @see InteractionRockfordRocher
	 * @see Deplacement
	 * 
	 * @see DetecteurInteractions
	 * @see DetecteurInteractions#estDetectee(Deplacement)
	 */
	@Override
	public boolean estDetectee(Deplacement d) {
		if (d.getObjet() instanceof Rockford && d.getPlateau().getObjet(d.getlArr(), d.getcArr()) instanceof Rocher) {
			setInteraction(new InteractionRockfordRocher(d));
			return true;
		}
		return false;
	}

	/**
	 * Retourne une instance de InteractionRockfordRocher
	 * 
	 * @return une instance de l'interaction
	 * 
	 * @see InteractionRockfordRocher
	 * 
	 * @see DetecteurInteractions
	 * @see DetecteurInteractions#extraire()
	 */
	@Override
	public InteractionRockfordRocher extraire() {
		return interaction;
	}

}
