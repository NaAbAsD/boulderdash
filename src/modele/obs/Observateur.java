package modele.obs;

/**
 * <b>Observateur est l'interface mod�le permettant de construire un expert capable de r�soudre un probl�me donn�.
 * Cette classe respecte le design pattern Observable/Observateur.</b>
 * 
 * @see Observable
 */
public interface Observateur {
	
	/**
	  * <p>
	  * recevoirNotification est la fonction appel�e lorsqu'un changement dans l'�tat du jeu est d�tect�e.
	  * C'est elle qui essayera de r�soudre le probl�me donn� par l'Observable.
	  * </p>
	  * 
	  * @throws Exception
	  * 
	  * @see Observateur
	  */
	public void recevoirNotification(Observable observable);

}
