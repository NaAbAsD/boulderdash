package modele.obs;

import java.util.ArrayList;
import modele.exceptions.BoulderException;

/**
 * <b>Observable est la classe contenant les experts n�cessaires � la r�solution de probl�mes d�tect�s.
 * Cette classe respecte le design pattern Observable/Observateur.</b>
 * <p>
 * Ce controleur est caract�ris� par l'information suivante :
 * <ul>
 * <li>L'ArrayList des diff�rents experts pour r�soudre les probl�mes.</li>
 * </ul>
 * </p>
 * 
 * @see ArrayList
 * @see Observateur
 */
public class Observable {
	
	/**
	 * La liste des experts � appeler
	 * 
	 * @see ArrayList
	 */
	private	ArrayList<Observateur> listeObservateur = new ArrayList<Observateur>();
	
	/**
	 * Constructeur de Observable
	 * 
	 */
	public Observable() {}
	
	/**
	  * <p>
	  * add est la fonction primitive de l'ArrayList qui permet d'ajouter nos experts � celle-ci.
	  * </p>
	  * 
	  * @throws Exception
	  * 
	  * @see ArrayList add
	  */
	public void add(Observateur obs) throws BoulderException {
		if (obs == null) {
			throw new BoulderException("Erreur : l'observateur est null");
		}
		listeObservateur.add(obs);		
	}
	
	/**
	  * <p>
	  * notifier est la fonction permettant d'appeler tour � tour les diff�rents experts capables de r�soudre notre probl�me.
	  * Cette fonction est appel�e lorsque un changement dans l'�tat du jeu est d�tect�.
	  * </p>
	  * 
	  * @throws Exception
	  * 
	  * @see Observateur
	  * @see Observateur#recevoirNotification(Observable)
	  */
	public void notifier() {
		for (Observateur observateur : listeObservateur) {
			observateur.recevoirNotification(this);
		}
	}
	
}
