package ui;

import javafx.geometry.Pos;
import javafx.scene.layout.HBox;

import modele.exceptions.BoulderException;
import modele.plateau.Plateau;
import ui.afficheurs.AfficheurDiamant;
import ui.afficheurs.AfficheurTempsEcoule;
import ui.afficheurs.AfficheurVie;
import ui.gestion.Diamant;
import ui.gestion.Timer;
import ui.gestion.Vie;

/**
 * <b>PanneauFooter est la classe repr�sentant la partie sous le canvas du jeu.</b>
 * <p>
 * Cette classe est caract�ris� par les informations suivantes :
 * <ul>
 * <li>Un Timer qui se d�clenche tous les 0.1 s.</li>
 * <li>Une instance de l'Objet Vie qui permet de mettre � jour le nombre de vies dans le PanneauFooter.</li>
 * <li>Une instance de l'Objet Diamants qui permet de mettre � jour le nombre de diamants dans le PanneauFooter.</li>
 * </ul>
 * </p>
 * 
 * @see Timer
 * @see Vie
 * @see Diamant
 * 
 */
public class PanneauFooter extends HBox {
	
	/**
	 * Timer qui se d�clenche tous les 0.1 s.
	 * 
	 * @see Timer
	 * 
	 * @see PanneauFooter#getTimer()
	 */
	private	Timer timer = new Timer(0.1);
	
	/**
	 * Une instance de l'Objet Vie qui permet de mettre � jour le nombre de vies dans le PanneauFooter.
	 * 
	 * @see Vie
	 * 
	 * @see PanneauFooter#getVie()
	 */
	private Vie vie = new Vie();
	
	/**
	 * Une instance de l'Objet Diamants qui permet de mettre � jour le nombre de diamants dans le PanneauFooter.
	 * 
	 * @see Diamant
	 * 
	 * @see PanneauFooter#getDiamant()
	 */
	private Diamant diamant = new Diamant();

	/**
	 * <b>Constructeur de PanneauFooter</b>
	 * <p>Construit une instance de PanneauFooter en cr�ant les diff�rents Afficheurs.</p>
	 * 
	 * @param plateau
	 * 
	 * @throws BoulderException
	 * 
	 * @see Plateau
	 * 
	 * @see AfficheurVie
	 * @see AfficheurDiamant
	 * @see AfficheurTempsEcoule
	 * 
	 * @see PanneauFooter#vie
	 * @see PanneauFooter#timer
	 * @see PanneauFooter#diamant
	 */
	public PanneauFooter(Plateau plateau) throws BoulderException {
		super();
		this.setAlignment(Pos.CENTER);
		this.setSpacing(40);
		AfficheurVie afficheVie = new AfficheurVie(this, plateau);
		AfficheurDiamant afficheDiamant = new AfficheurDiamant(this, plateau);
		AfficheurTempsEcoule afficheur = new AfficheurTempsEcoule(this);
		try {
			vie.add(afficheVie);
			timer.add(afficheur);
			diamant.add(afficheDiamant);
		} catch (BoulderException e) {
			throw e;
		}
	}
	
	/**
	 * <p>Retourne une instance de Timer correspondant aux temps de jeu de la partie.</p>
	 * 
	 * @return un timer.
	 * 
	 * @see Timer
	 * 
	 * @see PanneauFooter#timer
	 */
	public Timer getTimer() {
		return timer;
	}

	/**
	 * <p>Retourne une instance de Vie correspondant aux nombres de vies du joueur.</p>
	 * 
	 * @return une instance de Vie
	 * 
	 * @see Vie
	 * 
	 * @see PanneauFooter#vie
	 */
	public Vie getVie() {
		return vie;
	}

	/**
	 * <p>Retourne une instance de Diamant correspondant aux nombres de diamants du joueur.</p>
	 * 
	 * @return une instance de Diamant
	 * 
	 * @see Diamant
	 * 
	 * @see PanneauFooter#diamant
	 */
	public Diamant getDiamant() {
		return diamant;
	}
	
}
