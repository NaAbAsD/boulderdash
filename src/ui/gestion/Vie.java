package ui.gestion;

import modele.obs.Observable;

/**
 * <b>Vie est la classe repr�sentant la vie � afficher dans le PanneauFooter.</b>
 * 
 * @see Observable
 */
public class Vie extends Observable {
	
	/**
	 * Constructeur de Vie
	 */
	public Vie() {}
	
}
