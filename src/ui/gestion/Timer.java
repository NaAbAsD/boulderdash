package ui.gestion;

import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.util.Duration;
import modele.obs.Observable;

/**
 * <b>Timer est la classe repr�sentant le temps � afficher dans le PanneauFooter.</b>
 * <p>
 * Ce controleur est caract�ris� par les informations suivantes :
 * <ul>
 * <li>La Timeline correspondant au temps �coul�s.</li>
 * <li>Le double correspondant au nombre de secondes entre chaque 'top'.</li>
 * </ul>
 * </p>
 * 
 * @see Observable
 */
public class Timer extends Observable {
	
	/**
	 * <p>La Timeline correspondant au temps �coul�s.</p>
	 * 
	 * @see Timer#getTimeline()
	 */
	private Timeline timeline;
	
	/**
	 * <p>Le double correspondant au nombre de secondes entre chaque 'top'.</p>
	 * 
	 * @see Timer#getLaps()
	 */
	private	double laps;
	
	/**
	 * Constructeur d'un timer �partir d'une dur�e exprim�e en secondes. Tous les 
	 * laps de temps, l'�vnement EventTimer est d�clench�, et lui-m�me notifie � tous 
	 * ses observateurs
	 * 
	 * @param laps nombre de secondes entre chaque 'top'
	 * 
	 * @see Timer#laps
	 * @see Timer#timeline
	 */
	public Timer(double laps) {
		this.laps = laps;
		KeyFrame frameJeu = new KeyFrame(Duration.seconds(laps), new EventTimer());
		timeline = new Timeline(frameJeu);
		timeline.setCycleCount(Animation.INDEFINITE);
		timeline.play();
	}
	
	/**
	 * Retourne l'instance de la timeline correspondant au Timer
	 * 
	 * @return une instance de Timeline
	 * 
	 * @see Timer#timeline
	 */
	public Timeline getTimeline() {
		return timeline;
	}

	/**
	 * Retourne le double correspondant au nombre de secondes entre chaque 'top'
	 * 
	 * @return un double
	 * 
	 * @see Timer#laps
	 */
	public double getLaps() {
		return laps;
	}
	
	class EventTimer implements EventHandler<ActionEvent> {
		@Override
        public void handle(ActionEvent event) {
			notifier();
        }	
	}
	
}
