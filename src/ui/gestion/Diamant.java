package ui.gestion;

import modele.obs.Observable;

/**
 * <b>Diamant est la classe repr�sentant les diamants � afficher dans le PanneauFooter.</b>
 * 
 * @see Observable
 */
public class Diamant extends Observable {
	
	/**
	 * Constructeur de Diamant
	 */
	public Diamant() {}
	
}
