package ui.afficheurs;

import javafx.scene.control.Label;
import javafx.scene.layout.Pane;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import modele.obs.Observable;
import modele.obs.Observateur;
import modele.plateau.Plateau;

/**
 * <b>AfficheurDiamant est la classe repr�sentant l'afficheur du nombre de diamants dans le PanneauFooter.</b>
 * <p>
 * Ce controleur est caract�ris� par les informations suivantes :
 * <ul>
 * <li>Le Plateau dans lequel il faut ajouter l'AfficheurDiamant.</li>
 * <li>Le Label d�fini par AfficheurDiamant qui comporte le nombre de diamants.</li>
 * </ul>
 * </p>
 * 
 * @see Observateur
 */
public class AfficheurDiamant implements Observateur {
	
	/**
	 * <p>Le Plateau dans lequel il faut ajouter l'AfficheurDiamant.</p>
	 * 
	 * @see Plateau
	 * 
	 * @see AfficheurDiamant#getPlateau()
	 * @see AfficheurDiamant#setPlateau(Plateau)
	 */
	private Plateau plateau;
	
	/**
	 * <p>Le Label d�fini par AfficheurDiamant qui comporte le nombre de diamants.</p>
	 */
	private	Label labelDiamant = new Label("");
	
	/**
	 * Le String permettant de modifier le Label
	 */
	private final static String LABEL = "Nombre de diamants : ";

	/**
	 * <b>Constructeur de AfficheurDiamant</b>
	 * <p>Ajoute le texte du nombre de diamants au label de la classe AfficheurDiamant</p>
	 * 
	 * @param panneau
	 * @param plateau
	 * 
	 * @see Plateau
	 * 
	 * @see AfficheurDiamant#setPlateau(Plateau)
	 * @see AfficheurDiamant#labelDiamant
	 */
	public AfficheurDiamant(Pane panneau, Plateau plateau) {
		setPlateau(plateau);
		labelDiamant.setText("Nombre de diamants : "+ plateau.getJoueur().getNbDiamants());
		labelDiamant.setFont(Font.font("Calibri", FontPosture.REGULAR, 20));
		panneau.getChildren().add(labelDiamant);
	}

	/**
	 * <p>Retourne l'instance de Plateau de l'AfficheurDiamant.</p>
	 * 
	 * @return une instance de Plateau
	 * 
	 * @see Plateau
	 * 
	 * @see AfficheurDiamant#plateau
	 */
	public Plateau getPlateau() {
		return plateau;
	}

	/**
	 * <p>Met � jour l'instance de Plateau pour AfficheurDiamant</p>
	 * 
	 * @param plateau
	 * 
	 * @see Plateau
	 * 
	 * @see AfficheurDiamant#plateau
	 */
	public void setPlateau(Plateau plateau) {
		this.plateau = plateau;
	}

	/**
	 * <p>Notifie la classe AfficheurDiamant lorsqu'un changement est � effectuer</p>
	 * 
	 * @see Observable
	 * 
	 * @see AfficheurDiamant#labelDiamant
	 */
	@Override
	public void recevoirNotification(Observable observable) {
		String nbDiamant = String.format("%d", plateau.getJoueur().getNbDiamants());
		labelDiamant.setText(LABEL + nbDiamant);
	}
	
}
