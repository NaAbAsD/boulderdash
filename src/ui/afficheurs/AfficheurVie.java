package ui.afficheurs;

import javafx.scene.control.Label;
import javafx.scene.layout.Pane;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import modele.obs.Observable;
import modele.obs.Observateur;
import modele.plateau.Plateau;

/**
 * <b>AfficheurVie est la classe repr�sentant l'afficheur du nombre de vies dans le PanneauFooter.</b>
 * <p>
 * Ce controleur est caract�ris� par les informations suivantes :
 * <ul>
 * <li>Le Plateau dans lequel il faut ajouter l'AfficheurVie.</li>
 * <li>Le Label d�fini par AfficheurVie qui comporte le nombre de vies.</li>
 * </ul>
 * </p>
 * 
 * @see Observateur
 */
public class AfficheurVie implements Observateur {
	
	/**
	 * <p>Le Plateau dans lequel il faut ajouter l'AfficheurVie.</p>
	 * 
	 * @see Plateau
	 * 
	 * @see AfficheurVie#getPlateau()
	 * @see AfficheurVie#setPlateau(Plateau)
	 */
	private Plateau plateau;
	
	/**
	 * Le Label d�fini par AfficheurVie qui comporte le nombre de vies
	 */
	private	Label labelVie = new Label("");
	
	private final static String LABEL = "Nombre de vie : ";

	/**
	 * <b>Constructeur de AfficheurVie</b>
	 * <p>Ajoute le texte du nombre de vies au label de la classe AfficheurVie</p>
	 * 
	 * @param panneau
	 * @param plateau
	 * 
	 * @see Plateau
	 * 
	 * @see AfficheurVie#setPlateau(Plateau)
	 * @see AfficheurVie#labelVie
	 */
	public AfficheurVie(Pane panneau, Plateau plateau) {
		setPlateau(plateau);
		labelVie.setText("Nombre de vie : "+ plateau.getJoueur().getVie());
		labelVie.setFont(Font.font("Calibri", FontPosture.REGULAR, 20));
		panneau.getChildren().add(labelVie);
	}

	/**
	 * <p>Retourne l'instance du plateau utilis�e par AfficheurVie</p>
	 * 
	 * @return une instance de Plateau
	 * 
	 * @see Plateau
	 * 
	 * @see AfficheurVie#plateau
	 */
	public Plateau getPlateau() {
		return plateau;
	}

	/**
	 * <p>Met � jour l'instance du plateau utilis�e par AfficheurVie</p>
	 * 
	 * @param plateau
	 * 
	 * @see Plateau
	 * 
	 * @see AfficheurVie#plateau
	 */
	public void setPlateau(Plateau plateau) {
		this.plateau = plateau;
	}

	/**
	 * <p>Notifie la classe AfficheurVie lorsqu'un changement est � effectuer</p>
	 * 
	 * @see Observable
	 * 
	 * @see AfficheurVie#labelVie
	 */
	@Override
	public void recevoirNotification(Observable observable) {
		String nbVie = String.format("%d", plateau.getJoueur().getVie());
		labelVie.setText(LABEL+nbVie);
	}
	
}
