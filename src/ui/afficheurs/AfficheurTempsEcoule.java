package ui.afficheurs;

import javafx.scene.control.Label;
import javafx.scene.layout.Pane;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import modele.obs.Observable;
import modele.obs.Observateur;
import ui.gestion.Timer;

/**
 * <b>AfficheurTempsEcoule est la classe repr�sentant l'afficheur du nombre de diamants dans le PanneauFooter.</b>
 * <p>
 * Ce controleur est caract�ris� par les informations suivantes :
 * <ul>
 * <li>Le double permettant de compter les secondes de l'AfficheurTempsEcoule.</li>
 * <li>Le Label d�fini par AfficheurTempsEcoule qui comporte le Temps � afficher.</li>
 * </ul>
 * </p>
 * 
 * @see Observateur
 */
public class AfficheurTempsEcoule implements Observateur {
	
	/**
	 * Le double permettant de compter les secondes de l'AfficheurTempsEcoule.
	 */
	private	double secondes = 0;
	
	/**
	 * Le Label d�fini par AfficheurTempsEcoule qui comporte le Temps � afficher.
	 */
	private	Label labelTempsEcoule = new Label("");
	
	private final static String LABEL = "Temps �coul� : ";

	/**
	 * <b>Constructeur de AfficheurTempsEcoule</b>
	 * <p>Ajoute le texte du nombre de secondes au label de la classe AfficheurTempsEcoule</p>
	 * 
	 * @param panneau
	 * 
	 * @see AfficheurTempsEcoule#labelTempsEcoule
	 */
	public AfficheurTempsEcoule(Pane panneau) {
		labelTempsEcoule.setFont(Font.font("Calibri", FontPosture.REGULAR, 20));
		panneau.getChildren().add(labelTempsEcoule);
	}

	/**
	 * <p>Notifie la classe AfficheurTempsEcoule lorsqu'un changement est � effectuer</p>
	 * 
	 * @see Observable
	 * 
	 * @see Timer
	 * 
	 * @see AfficheurTempsEcoule
	 */
	@Override
	public void recevoirNotification(Observable observable) {
		Timer timer = (Timer)observable;
		secondes += timer.getLaps();
		
		// YL : astuces pour formater un réel avec 1 seul chiffre après la virgule
		String strSecondes = String.format("%.1f", secondes);
		
		labelTempsEcoule.setText(LABEL + strSecondes);
	}
	
}
